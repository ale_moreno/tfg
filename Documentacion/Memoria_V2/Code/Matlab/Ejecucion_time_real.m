SerialPort='com3'; % serial port

N=200;
Fs=200;

m=zeros(1,N);

s = serial(SerialPort);
set(s,'BaudRate',57600);
try
    fopen(s);
catch err
    fclose(instrfind);
    error('Make sure you select the correct COM Port where the Arduino is connected.');
end
 
KeepRunning = 1;
try
    while KeepRunning
        for i = 1:N
            datum = fscanf(s, '%s');
            fprintf('%s\n', datum);

            if (length(datum) > 0)
                m(i) = str2num(datum);
            else
                m(i) = 0;
            end
            
            subplot(1,2,1)
            hLine = plot(m);
            ylim([0 1024]);
            set(hLine,'YData',m);
            drawnow
            subplot(1,2,2)
%             pwelch(m,[],[],[],Fs);
            FFT_code;
            drawnow

            if (ishandle(1))
                set(hLine,'YData',m);
            else
                KeepRunning = 0;
                break;
            end
        end
        i=0;
    end

catch err
    fclose(s);
    delete(s);
end

clear s;
  
FileName=['Ejecucion_real_time',datestr(now, 'dd-mmm-yyyy'),num2str(randi([0 20],1,1))];
save(FileName)