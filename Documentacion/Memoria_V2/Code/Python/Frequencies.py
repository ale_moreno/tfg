from numpy import fft


class Frequencies:
    def __init__(self):

        self.N = 200

        self.Delta = 0.0
        self.Theta = 0.0
        self.Alpha = 0.0
        self.Beta = 0.0
        self.Gamma = 0.0

        self._FFT = []

    def get_alpha(self):
        return self.Alpha

    def get_beta(self):
        return self.Beta

    def get_theta(self):
        return self.Theta

    def get_delta(self):
        return self.Delta

    def get_gamma(self):
        return self.Gamma

    def set_delta(self, data):
        self.Delta = data

    def set_theta(self,data):
        self.Theta = data

    def set_alpha(self,data):
        self.Alpha = data

    def set_beta(self,data):
        self.Beta = data

    def set_gamma(self,data):
        self.Gamma = data