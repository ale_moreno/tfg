\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivaci\IeC {\'o}n}{9}{section.1.1}
\contentsline {section}{\numberline {1.2}Estado del arte}{9}{section.1.2}
\contentsline {chapter}{\numberline {2}Conocimientos previos de Neurolog\IeC {\'\i }a}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}El cerebro humano}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Electroencefalograma}{12}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Posicionamiento de los electrodos}{13}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Configuraci\IeC {\'o}n y montaje de electrodos}{13}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Ondas cerebrales}{14}{subsection.2.2.3}
\contentsline {subsubsection}{Ondas Delta}{14}{section*.9}
\contentsline {subsubsection}{Ondas Theta}{15}{section*.11}
\contentsline {subsubsection}{Ondas Alpha}{15}{section*.13}
\contentsline {subsubsection}{Ondas Beta}{15}{section*.15}
\contentsline {subsubsection}{Ondas Gamma}{16}{section*.16}
\contentsline {section}{\numberline {2.3}Migra\IeC {\~n}a}{16}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Anomal\IeC {\'\i }as frecuentes}{17}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Memoria descriptiva}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Hardware}{18}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Sensores}{18}{subsection.3.1.1}
\contentsline {subsubsection}{OpenBCI EEG Headband Kit}{19}{section*.21}
\contentsline {subsubsection}{EMG/ECG Snap Electrode Cables}{20}{section*.23}
\contentsline {subsection}{\numberline {3.1.2}Placa de desarrollo}{20}{subsection.3.1.2}
\contentsline {subsubsection}{Teensy 3.6}{21}{section*.26}
\contentsline {subsection}{\numberline {3.1.3}Teensy 3 - Bluetooth LE 4.0 Module}{22}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Esquem\IeC {\'a}tico}{22}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Circuito amplificador}{23}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Simulaci\IeC {\'o}n}{24}{section.3.3}
\contentsline {section}{\numberline {3.4}Protoboard}{25}{section.3.4}
\contentsline {section}{\numberline {3.5}Osciloscopio virtual}{27}{section.3.5}
\contentsline {section}{\numberline {3.6}Transformada de Fourier}{28}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Short-Time Fourier Transform}{28}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Fast Fourier Transform}{30}{subsection.3.6.2}
\contentsline {section}{\numberline {3.7}Estudio con Matlab}{31}{section.3.7}
\contentsline {section}{\numberline {3.8}Aplicaci\IeC {\'o}n Python}{32}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Funcionamiento}{33}{subsection.3.8.1}
\contentsline {subsubsection}{Get\_data}{34}{section*.43}
\contentsline {subsubsection}{Frequencies}{35}{section*.46}
\contentsline {subsubsection}{Main}{36}{section*.48}
\contentsline {section}{\numberline {3.9}C\IeC {\'o}digo Teensy}{39}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}Variables}{40}{subsection.3.9.1}
\contentsline {subsection}{\numberline {3.9.2}Recolecci\IeC {\'o}n datos}{41}{subsection.3.9.2}
\contentsline {subsubsection}{Llamada a la funci\IeC {\'o}n}{41}{section*.56}
\contentsline {subsubsection}{C\IeC {\'a}lculos}{41}{section*.58}
\contentsline {subsubsection}{Elecci\IeC {\'o}n del canal}{42}{section*.60}
\contentsline {subsection}{\numberline {3.9.3}FFT}{43}{subsection.3.9.3}
\contentsline {subsection}{\numberline {3.9.4}Subfrecuencias}{43}{subsection.3.9.4}
\contentsline {chapter}{\numberline {4}Experimentaci\IeC {\'o}n}{45}{chapter.4}
\contentsline {section}{\numberline {4.1}Dise\IeC {\~n}o y construcci\IeC {\'o}n de placa}{45}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Kicad}{45}{subsection.4.1.1}
\contentsline {subsubsection}{Impresi\IeC {\'o}n}{46}{section*.67}
\contentsline {subsection}{\numberline {4.1.2}Error de impresi\IeC {\'o}n}{48}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Obtenci\IeC {\'o}n de datos}{49}{section.4.2}
\contentsline {section}{\numberline {4.3}Pruebas de ejercicios}{49}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Usuario 1}{49}{subsection.4.3.1}
\contentsline {subsubsection}{11:46}{50}{section*.72}
\contentsline {subsubsection}{11:48}{51}{section*.75}
\contentsline {subsection}{\numberline {4.3.2}Usuario 2}{52}{subsection.4.3.2}
\contentsline {subsubsection}{12:08}{52}{section*.78}
\contentsline {subsubsection}{12:10}{53}{section*.81}
\contentsline {section}{\numberline {4.4}M\IeC {\'u}sica estridente}{54}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Usuario 1}{54}{subsection.4.4.1}
\contentsline {subsubsection}{11:51}{54}{section*.84}
\contentsline {subsubsection}{11:53}{56}{section*.87}
\contentsline {subsubsection}{11:55}{57}{section*.90}
\contentsline {subsection}{\numberline {4.4.2}Usuario 2}{58}{subsection.4.4.2}
\contentsline {subsubsection}{12:13}{59}{section*.94}
\contentsline {subsubsection}{12:14}{60}{section*.97}
\contentsline {subsubsection}{12:16}{61}{section*.100}
\contentsline {section}{\numberline {4.5}M\IeC {\'u}sica relajante}{62}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Usuario 1}{62}{subsection.4.5.1}
\contentsline {subsubsection}{11:58}{63}{section*.103}
\contentsline {subsubsection}{11:59}{63}{section*.106}
\contentsline {subsubsection}{12:01}{65}{section*.109}
\contentsline {subsection}{\numberline {4.5.2}Usuario 2}{66}{subsection.4.5.2}
\contentsline {subsubsection}{12:17}{66}{section*.112}
\contentsline {subsubsection}{12:19}{67}{section*.115}
\contentsline {subsubsection}{12:20}{68}{section*.118}
\contentsline {section}{\numberline {4.6}Conclusiones}{69}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Prueba de ejercicio}{69}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}M\IeC {\'u}sica estridente}{70}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}M\IeC {\'u}sica relajante}{71}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}Conclusi\IeC {\'o}n}{72}{subsection.4.6.4}
\contentsline {chapter}{\numberline {5}Conclusiones}{73}{chapter.5}
\contentsline {section}{\numberline {5.1}Mejoras}{73}{section.5.1}
\contentsline {chapter}{\numberline {A}C\IeC {\'o}digo Matlab}{87}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}C\IeC {\'a}lculo de frecuencias}{87}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Ejecuci\IeC {\'o}n tiempo real}{88}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Estudio de datos}{89}{section.Alph1.3}
\contentsline {section}{\numberline {A.4}FFT}{92}{section.Alph1.4}
\contentsline {section}{\numberline {A.5}Recolecci\IeC {\'o}n de de datos}{93}{section.Alph1.5}
\contentsline {section}{\numberline {A.6}Recolecci\IeC {\'o}n de datos por minuto}{95}{section.Alph1.6}
\contentsline {section}{\numberline {A.7}Comprobaci\IeC {\'o}n del serial}{97}{section.Alph1.7}
\contentsline {chapter}{\numberline {B}C\IeC {\'o}digo en Python}{98}{appendix.Alph2}
\contentsline {section}{\numberline {B.1}Main.py}{98}{section.Alph2.1}
\contentsline {section}{\numberline {B.2}Get\_data.py}{102}{section.Alph2.2}
\contentsline {section}{\numberline {B.3}Frequencies}{103}{section.Alph2.3}
\contentsline {chapter}{\numberline {C}C\IeC {\'o}digo en Teensy}{105}{appendix.Alph3}
\contentsline {chapter}{\numberline {D}Reacondicionamiento de la se\IeC {\~n}al}{109}{appendix.Alph4}
\contentsline {section}{\numberline {D.1}Filtro paso alto}{110}{section.Alph4.1}
\contentsline {section}{\numberline {D.2}Filtro paso bajo activo}{111}{section.Alph4.2}
\contentsline {section}{\numberline {D.3}Filtro paso bajo pasivo}{112}{section.Alph4.3}
\contentsline {section}{\numberline {D.4}Gr\IeC {\'a}fica filtro paso alto pasivo}{113}{section.Alph4.4}
\contentsline {section}{\numberline {D.5}Gr\IeC {\'a}fica filtro paso bajo activo}{113}{section.Alph4.5}
\contentsline {section}{\numberline {D.6}Gr\IeC {\'a}fica filtro paso bajo pasivo}{114}{section.Alph4.6}
\contentsline {section}{\numberline {D.7}Gr\IeC {\'a}fica filtro paso banda}{115}{section.Alph4.7}
