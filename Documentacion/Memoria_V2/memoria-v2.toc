\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivaci\IeC {\'o}n}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Estado del arte}{5}{section.1.2}
\contentsline {chapter}{\numberline {2}Conocimientos previos de Neurolog\IeC {\'\i }a}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}El cerebro humano}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Electroencefalograma}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Posicionamiento de los electrodos}{9}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Configuraci\IeC {\'o}n y montaje de electrodos}{9}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Ondas cerebrales}{10}{subsection.2.2.3}
\contentsline {subsubsection}{Ondas Delta}{10}{section*.9}
\contentsline {subsubsection}{Ondas Theta}{11}{section*.11}
\contentsline {subsubsection}{Ondas Alpha}{11}{section*.13}
\contentsline {subsubsection}{Ondas Beta}{11}{section*.15}
\contentsline {subsubsection}{Ondas Gamma}{12}{section*.16}
\contentsline {section}{\numberline {2.3}Migra\IeC {\~n}a}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Anomal\IeC {\'\i }as frecuentes}{13}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Memoria descriptiva}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Hardware}{14}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Sensores}{14}{subsection.3.1.1}
\contentsline {subsubsection}{OpenBCI EEG Headband Kit}{15}{section*.21}
\contentsline {subsubsection}{EMG/ECG Snap Electrode Cables}{16}{section*.23}
\contentsline {subsection}{\numberline {3.1.2}Placa de desarrollo}{16}{subsection.3.1.2}
\contentsline {subsubsection}{Teensy 3.6}{17}{section*.26}
\contentsline {subsection}{\numberline {3.1.3}Teensy 3 - Bluetooth LE 4.0 Module}{18}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Esquem\IeC {\'a}tico}{18}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Circuito amplificador}{19}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Simulaci\IeC {\'o}n}{20}{section.3.3}
\contentsline {section}{\numberline {3.4}Protoboard}{21}{section.3.4}
\contentsline {section}{\numberline {3.5}Osciloscopio virtual}{23}{section.3.5}
\contentsline {section}{\numberline {3.6}Transformada de Fourier}{24}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Short-Time Fourier Transform}{24}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Fast Fourier Transform}{26}{subsection.3.6.2}
\contentsline {section}{\numberline {3.7}Estudio con Matlab}{27}{section.3.7}
\contentsline {section}{\numberline {3.8}Aplicaci\IeC {\'o}n Python}{28}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Funcionamiento}{29}{subsection.3.8.1}
\contentsline {subsubsection}{Get\_data}{30}{section*.43}
\contentsline {subsubsection}{Frequencies}{31}{section*.46}
\contentsline {subsubsection}{Main}{32}{section*.48}
\contentsline {section}{\numberline {3.9}C\IeC {\'o}digo Teensy}{35}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}Variables}{36}{subsection.3.9.1}
\contentsline {subsection}{\numberline {3.9.2}Recolecci\IeC {\'o}n datos}{37}{subsection.3.9.2}
\contentsline {subsubsection}{Llamada a la funci\IeC {\'o}n}{37}{section*.56}
\contentsline {subsubsection}{C\IeC {\'a}lculos}{37}{section*.58}
\contentsline {subsubsection}{Elecci\IeC {\'o}n del canal}{38}{section*.60}
\contentsline {subsection}{\numberline {3.9.3}FFT}{39}{subsection.3.9.3}
\contentsline {subsection}{\numberline {3.9.4}Subfrecuencias}{39}{subsection.3.9.4}
\contentsline {chapter}{\numberline {4}Experimentaci\IeC {\'o}n}{41}{chapter.4}
\contentsline {section}{\numberline {4.1}Dise\IeC {\~n}o y construcci\IeC {\'o}n de placa}{41}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Kicad}{41}{subsection.4.1.1}
\contentsline {subsubsection}{Impresi\IeC {\'o}n}{42}{section*.67}
\contentsline {subsection}{\numberline {4.1.2}Error de impresi\IeC {\'o}n}{44}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Obtenci\IeC {\'o}n de datos}{45}{section.4.2}
\contentsline {section}{\numberline {4.3}Pruebas de ejercicios}{45}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Usuario 1}{45}{subsection.4.3.1}
\contentsline {subsubsection}{11:46}{46}{section*.72}
\contentsline {subsubsection}{11:48}{47}{section*.75}
\contentsline {subsection}{\numberline {4.3.2}Usuario 2}{48}{subsection.4.3.2}
\contentsline {subsubsection}{12:08}{48}{section*.78}
\contentsline {subsubsection}{12:10}{49}{section*.81}
\contentsline {section}{\numberline {4.4}M\IeC {\'u}sica estridente}{50}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Usuario 1}{50}{subsection.4.4.1}
\contentsline {subsubsection}{11:51}{50}{section*.84}
\contentsline {subsubsection}{11:53}{52}{section*.87}
\contentsline {subsubsection}{11:55}{53}{section*.90}
\contentsline {subsection}{\numberline {4.4.2}Usuario 2}{54}{subsection.4.4.2}
\contentsline {subsubsection}{12:13}{55}{section*.94}
\contentsline {subsubsection}{12:14}{56}{section*.97}
\contentsline {subsubsection}{12:16}{57}{section*.100}
\contentsline {section}{\numberline {4.5}M\IeC {\'u}sica relajante}{58}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Usuario 1}{58}{subsection.4.5.1}
\contentsline {subsubsection}{11:58}{59}{section*.103}
\contentsline {subsubsection}{11:59}{59}{section*.106}
\contentsline {subsubsection}{12:01}{61}{section*.109}
\contentsline {subsection}{\numberline {4.5.2}Usuario 2}{62}{subsection.4.5.2}
\contentsline {subsubsection}{12:17}{62}{section*.112}
\contentsline {subsubsection}{12:19}{63}{section*.115}
\contentsline {subsubsection}{12:20}{64}{section*.118}
\contentsline {section}{\numberline {4.6}Conclusiones}{65}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Prueba de ejercicio}{65}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}M\IeC {\'u}sica estridente}{66}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}M\IeC {\'u}sica relajante}{67}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}Conclusi\IeC {\'o}n}{68}{subsection.4.6.4}
\contentsline {chapter}{\numberline {5}Conclusiones}{69}{chapter.5}
\contentsline {section}{\numberline {5.1}Mejoras}{69}{section.5.1}
\contentsline {chapter}{\numberline {A}C\IeC {\'o}digo Matlab}{80}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}C\IeC {\'a}lculo de frecuencias}{80}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Ejecuci\IeC {\'o}n tiempo real}{81}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Estudio de datos}{82}{section.Alph1.3}
\contentsline {section}{\numberline {A.4}FFT}{85}{section.Alph1.4}
\contentsline {section}{\numberline {A.5}Recolecci\IeC {\'o}n de de datos}{86}{section.Alph1.5}
\contentsline {section}{\numberline {A.6}Recolecci\IeC {\'o}n de datos por minuto}{88}{section.Alph1.6}
\contentsline {section}{\numberline {A.7}Comprobaci\IeC {\'o}n del serial}{90}{section.Alph1.7}
\contentsline {chapter}{\numberline {B}C\IeC {\'o}digo en Python}{91}{appendix.Alph2}
\contentsline {section}{\numberline {B.1}Main.py}{91}{section.Alph2.1}
\contentsline {section}{\numberline {B.2}Get\_data.py}{95}{section.Alph2.2}
\contentsline {section}{\numberline {B.3}Frequencies}{96}{section.Alph2.3}
\contentsline {chapter}{\numberline {C}C\IeC {\'o}digo en Teensy}{98}{appendix.Alph3}
\contentsline {chapter}{\numberline {D}Reacondicionamiento de la se\IeC {\~n}al}{102}{appendix.Alph4}
\contentsline {section}{\numberline {D.1}Filtro paso alto}{103}{section.Alph4.1}
\contentsline {section}{\numberline {D.2}Filtro paso bajo activo}{104}{section.Alph4.2}
\contentsline {section}{\numberline {D.3}Filtro paso bajo pasivo}{105}{section.Alph4.3}
\contentsline {section}{\numberline {D.4}Gr\IeC {\'a}fica filtro paso alto pasivo}{106}{section.Alph4.4}
\contentsline {section}{\numberline {D.5}Gr\IeC {\'a}fica filtro paso bajo activo}{106}{section.Alph4.5}
\contentsline {section}{\numberline {D.6}Gr\IeC {\'a}fica filtro paso bajo pasivo}{107}{section.Alph4.6}
\contentsline {section}{\numberline {D.7}Gr\IeC {\'a}fica filtro paso banda}{108}{section.Alph4.7}
