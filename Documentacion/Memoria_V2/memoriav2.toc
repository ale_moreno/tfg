\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Resumen}{9}{section.1.1}
\contentsline {section}{\numberline {1.2}Abstract}{10}{section.1.2}
\contentsline {section}{\numberline {1.3}Motivaci\IeC {\'o}n}{11}{section.1.3}
\contentsline {section}{\numberline {1.4}Estado del arte}{11}{section.1.4}
\contentsline {section}{\numberline {1.5}Fundamentos de Neurolog\IeC {\'\i }a}{14}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}El cerebro humano}{14}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Electroencefalograma}{15}{subsection.1.5.2}
\contentsline {subsubsection}{Posicionamiento de los electrodos}{15}{section*.9}
\contentsline {subsection}{\numberline {1.5.3}Configuraci\IeC {\'o}n y montaje de electrodos}{16}{subsection.1.5.3}
\contentsline {subsection}{\numberline {1.5.4}Ondas cerebrales}{17}{subsection.1.5.4}
\contentsline {subsubsection}{Ondas Delta}{18}{section*.14}
\contentsline {subsubsection}{Ondas Alpha}{19}{section*.18}
\contentsline {chapter}{\numberline {2}Memoria descriptiva}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Hardware}{21}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Sensores}{21}{subsection.2.1.1}
\contentsline {subsubsection}{OpenBCI EEG Headband Kit}{22}{section*.22}
\contentsline {subsubsection}{EMG/ECG Snap Electrode Cables}{23}{section*.24}
\contentsline {subsection}{\numberline {2.1.2}Placa de desarrollo}{23}{subsection.2.1.2}
\contentsline {subsubsection}{Teensy 3.6}{24}{section*.27}
\contentsline {subsection}{\numberline {2.1.3}Teensy 3 - Bluetooth LE 4.0 Module}{26}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Esquem\IeC {\'a}tico}{26}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Circuito amplificador}{28}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Dise\IeC {\~n}o y construcci\IeC {\'o}n de placa}{29}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Kicad}{29}{subsection.2.3.1}
\contentsline {subsubsection}{Impresi\IeC {\'o}n}{30}{section*.35}
\contentsline {subsection}{\numberline {2.3.2}Error de impresi\IeC {\'o}n}{32}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Simulaci\IeC {\'o}n}{33}{section.2.4}
\contentsline {section}{\numberline {2.5}Protoboard}{34}{section.2.5}
\contentsline {section}{\numberline {2.6}Osciloscopio virtual}{35}{section.2.6}
\contentsline {section}{\numberline {2.7}Transformada de Fourier}{36}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Short-Time Fourier Transform}{37}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Fast Fourier Transform}{39}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}Estudio con Matlab}{40}{section.2.8}
\contentsline {section}{\numberline {2.9}C\IeC {\'o}digo Teensy}{41}{section.2.9}
\contentsline {subsection}{\numberline {2.9.1}Variables}{43}{subsection.2.9.1}
\contentsline {subsection}{\numberline {2.9.2}Funcionamiento}{43}{subsection.2.9.2}
\contentsline {section}{\numberline {2.10}Aplicaci\IeC {\'o}n Python}{45}{section.2.10}
\contentsline {subsection}{\numberline {2.10.1}Funcionamiento}{46}{subsection.2.10.1}
\contentsline {chapter}{\numberline {3}Experimentaci\IeC {\'o}n}{49}{chapter.3}
\contentsline {section}{\numberline {3.1}Obtenci\IeC {\'o}n de datos}{49}{section.3.1}
\contentsline {section}{\numberline {3.2}Pruebas de ejercicios matem\IeC {\'a}ticos}{49}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Usuario 1}{50}{subsection.3.2.1}
\contentsline {subsubsection}{Primera toma de datos del Usuario 1}{50}{section*.58}
\contentsline {subsubsection}{Segunda toma de datos del Usuario 1}{51}{section*.61}
\contentsline {subsection}{\numberline {3.2.2}Usuario 2}{52}{subsection.3.2.2}
\contentsline {subsubsection}{Primera toma de datos del Usuario 2}{52}{section*.64}
\contentsline {subsubsection}{Segunda toma de datos del Usuario 2}{53}{section*.67}
\contentsline {section}{\numberline {3.3}M\IeC {\'u}sica estridente}{54}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Usuario 1}{54}{subsection.3.3.1}
\contentsline {subsubsection}{Segunda toma de datos del Usuario 1}{55}{section*.70}
\contentsline {subsubsection}{Tercera toma de datos del Usuario 1}{56}{section*.73}
\contentsline {subsection}{\numberline {3.3.2}Usuario 2}{57}{subsection.3.3.2}
\contentsline {subsubsection}{Segunda toma de datos del Usuario 2}{57}{section*.77}
\contentsline {subsubsection}{Tercera toma de datos del Usuario 2}{59}{section*.80}
\contentsline {section}{\numberline {3.4}M\IeC {\'u}sica relajante}{60}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Usuario 1}{60}{subsection.3.4.1}
\contentsline {subsubsection}{Primera toma de datos del Usuario 1}{60}{section*.83}
\contentsline {subsubsection}{Segunda toma de datos del Usuario 1}{61}{section*.86}
\contentsline {subsubsection}{Tercera toma de datos del Usuario 1}{62}{section*.89}
\contentsline {subsection}{\numberline {3.4.2}Usuario 2}{63}{subsection.3.4.2}
\contentsline {subsubsection}{Primera toma de datos del Usuario 2}{63}{section*.92}
\contentsline {subsubsection}{Segunda toma de datos del Usuario 2}{64}{section*.95}
\contentsline {subsubsection}{Tercera toma de datos del Usuario 2}{65}{section*.98}
\contentsline {section}{\numberline {3.5}Conclusiones}{66}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Prueba de ejercicio matem\IeC {\'a}tico}{66}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}M\IeC {\'u}sica estridente}{67}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}M\IeC {\'u}sica relajante}{68}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Conclusi\IeC {\'o}n}{69}{subsection.3.5.4}
\contentsline {chapter}{\numberline {4}Conclusiones}{71}{chapter.4}
\contentsline {section}{\numberline {4.1}Implementaci\IeC {\'o}n y coste}{72}{section.4.1}
\contentsline {section}{\numberline {4.2}Trabajos futuros}{72}{section.4.2}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{75}{section.4.2}
\contentsline {chapter}{\numberline {A}Reacondicionamiento de la se\IeC {\~n}al}{89}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Filtro paso alto}{90}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Filtro paso bajo activo}{91}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Filtro paso bajo pasivo}{92}{section.Alph1.3}
\contentsline {section}{\numberline {A.4}Gr\IeC {\'a}fica filtro paso alto pasivo}{93}{section.Alph1.4}
\contentsline {section}{\numberline {A.5}Gr\IeC {\'a}fica filtro paso bajo activo}{93}{section.Alph1.5}
\contentsline {section}{\numberline {A.6}Gr\IeC {\'a}fica filtro paso bajo pasivo}{94}{section.Alph1.6}
\contentsline {section}{\numberline {A.7}Gr\IeC {\'a}fica filtro paso banda}{95}{section.Alph1.7}
\contentsline {chapter}{\numberline {B}C\IeC {\'o}digo Matlab}{97}{appendix.Alph2}
\contentsline {section}{\numberline {B.1}C\IeC {\'a}lculo de frecuencias}{97}{section.Alph2.1}
\contentsline {section}{\numberline {B.2}Ejecuci\IeC {\'o}n tiempo real}{98}{section.Alph2.2}
\contentsline {section}{\numberline {B.3}Estudio de datos}{99}{section.Alph2.3}
\contentsline {section}{\numberline {B.4}FFT}{102}{section.Alph2.4}
\contentsline {section}{\numberline {B.5}Recolecci\IeC {\'o}n de datos}{103}{section.Alph2.5}
\contentsline {section}{\numberline {B.6}Recolecci\IeC {\'o}n de datos por minuto}{105}{section.Alph2.6}
\contentsline {section}{\numberline {B.7}Comprobaci\IeC {\'o}n del serial}{106}{section.Alph2.7}
\contentsline {chapter}{\numberline {C}C\IeC {\'o}digo en Python}{109}{appendix.Alph3}
\contentsline {section}{\numberline {C.1}Main.py}{109}{section.Alph3.1}
\contentsline {section}{\numberline {C.2}Get\_data.py}{113}{section.Alph3.2}
\contentsline {section}{\numberline {C.3}Frequencies}{114}{section.Alph3.3}
\contentsline {chapter}{\numberline {D}C\IeC {\'o}digo en Teensy}{117}{appendix.Alph4}
