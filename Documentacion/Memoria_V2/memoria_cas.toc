\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Migra\IeC {\~n}a}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}Componentes}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Sensores}{8}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}OpenBCI EEG Headband Kit}{9}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}EMG/ECG Snap Electrode Cables}{9}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Placa de desarrollo}{10}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Teensy 3.6}{10}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Teensy 3 - Bluetooth LE 4.0 Module}{12}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Circuito adaptador}{13}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Circuito amplificador}{14}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Presupuesto}{16}{chapter.3}
\contentsline {chapter}{\numberline {4}Anexo}{17}{chapter.4}
