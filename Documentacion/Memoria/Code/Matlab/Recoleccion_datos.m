clear all;
close all;
clc

s = serial('COM3');
set(s,'BaudRate',57600);
try
    fopen(s);
catch err
    fclose(instrfind);
    error('Make sure you select the correct COM Port where the Arduino is connected.');
end

Tmax = 1000; % Total time for data collection (s)
Fs = 100;
figure,
grid on,
xlabel ('Time (s)'), ylabel('Data (8-bit)'),
axis([0 Tmax+1 -10 500]),
%% Read and plot the data from Arduino
Ts = 0.02; % Sampling time (s)
i = 0;
data = 0;
t = 0;
tic % Start timer
while toc <= Tmax
    i = i + 1;
    %% Read buffer data
    datum = fscanf(s, '%s');
    
      if (length(datum) > 0)
            data(i) = str2num(datum);
        else
            data(i) = 0;
      end
      
    fprintf('%s\n', data(i));
    %% Read time stamp
    % If reading faster than sampling rate, force sampling time.
    % If reading slower than sampling rate, nothing can be done. Consider
    % decreasing the set sampling time Ts
    t(i) = toc;
    if i > 1
        T = toc - t(i-1);
        while T < Ts
            T = toc - t(i-1);
        end
    end
    t(i) = toc;
    %% Plot live data
    if i > 1
        line([t(i-1) t(i)],[data(i-1) data(i)])
        drawnow
    end
    %% plot spectrogram real time
    try
        
       if i > 1
           % Estudio con ventana muestral menos 1
%             Window = i-1
%             % Solapamiento es la ventana
%             overlap = Window;
%             [s,f,t]=spectrogram(data,i,Window,overlap,Fs)
%             title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);
%             line([f(i-2) f(i-1)],[s(i-2) s(i-1)])
%             drawnow
%             figure
%             pwelch(m,[],[],[],Fs);
%             drawnow
       end
    catch err
        fclose(s);
    end
      
end
fclose(s);

figure(3)
L=length(data);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(data,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
 
% Plot single-sided amplitude spectrum.
plot(f,2*abs(Y(1:NFFT/2+1))) 
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|') 


calculo_frecuencias;
FileName=['Workspace',datestr(now, 'dd-mmm-yyyy'),num2str(randi([0 20],1,1))];
save(FileName)