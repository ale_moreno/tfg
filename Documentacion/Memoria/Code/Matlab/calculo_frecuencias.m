alpha=0;beta=0;delta=0;gamma=0;theta=0;

total=0;

cont=1;
cont25=1;
for i=1:length(f)
    if f(i) >= 0.5
        total = total + P1(cont);
    end
end

cont=1;
while f(cont)<4
    if(f(cont)<0.6)
        cont=cont+1;
    else
        delta = delta + P1(cont);
        cont=cont+1;
    end
end


while f(cont)<=8
    theta = theta + P1(cont);
    cont=cont+1;
end

while f(cont)<=13
    alpha = alpha + P1(cont);
    cont=cont+1;
end

llega_cont = 1;
while f(cont)<=30
    beta = beta + P1(cont);
    cont = cont+1;
end

while f(cont)<=60.5
    gamma = gamma + P1(cont);
    cont = cont+1;
end

alpha = alpha/total;
beta = beta/total;
gamma = gamma/total;
theta = theta/total;
delta = delta/total;

figure(3);
frecuencies = [delta theta alpha beta gamma];
c = {'delta','theta','alpha','beta','gamma'};
h=bar(frecuencies);
set(gca,'XTickLabel',c)