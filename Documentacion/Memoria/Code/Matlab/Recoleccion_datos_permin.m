clear all;
close all;
clc

s = serial('COM6');
set(s,'BaudRate',57600);
try
    fopen(s);
catch err
    fclose(instrfind);
    error('Make sure you select the correct COM Port where the Arduino is connected.');
end

data = 0;
KeepRunning = 1;
figure(1);
hLine=plot(data);
title(['Recoleccion de datos' datestr(now)]);

try
    while KeepRunning
        Tmax = 90;
        Ts = 0.02; % Sampling time (s)
        Fs = 200;
        i = 0;
        clear data;
        t = 0;
        tic % Start timer
        while toc <= Tmax
            i = i + 1;
            %% Read buffer data
            datum = fscanf(s, '%s');
            if (~isempty(datum))
            	data(i) = str2num(datum);
            else
                data(i) = 0;
            end
            fprintf('%s\n', data(i));
            %% Read time stamp
            % If reading faster than sampling rate, force sampling time.
            % If reading slower than sampling rate, nothing can be done. Consider
            % decreasing the set sampling time Ts
            t(i) = toc;
            if i > 1
                T = toc - t(i-1);
                while T < Ts
                    T = toc - t(i-1);
                end
            end
            t(i) = toc;           
            
            %% Plot live data
            if i > 1
                figure(2);
                line([t(i-1) t(i)],[data(i-1) data(i)]);
                title(['Recoleccion de datos' datestr(now)]);
                drawnow
            end
            
             if (ishandle(1))
                set(hLine,'YData',data);
            else
                KeepRunning = 0;
                break;
            end
        end
        m = data;
        FFT_code;
        
        FileName=['Workspace',datestr(now, 'dd-mmm-yyyy-HH-MM-SS')];
        save(FileName)
    end

catch err
    fclose(s);
    delete(s);
end
