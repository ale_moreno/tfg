import serial
from Frequencies import Frequencies

def get_data(Channel0,Channel1,Channel2,Channel3, 
    Channel4, Run, Frequencies):
    global ser
    print("Has llamado a Get_data")

    try:
        ser = serial.Serial('COM6', 9600)
    except NameError:
        print("Error al ejecutar serial, Serial no conectado" + NameError)

    while True:
        if Run == 1:
            pulldata = ser.readline().decode('utf-8')
            print(pulldata)
            get_data = pulldata.split(',')
            # Using 5 channels
            Channel0.append(float(get_data[0]))
            Channel1.append(float(get_data[1]))
            Channel2.append(float(get_data[2]))
            Channel3.append(float(get_data[3]))
            Channel4.append(float(get_data[4]))
            Frequencies.set_delta(float(get_data[5]))
            Frequencies.set_theta(float(get_data[6]))
            Frequencies.set_alpha(float(get_data[7]))
            Frequencies.set_beta(float(get_data[8]))
            Frequencies.set_gamma(float(get_data[9]))
            #
            # Using only one channel
            #
            # Channel0.append(float(get_data[0]))
            # Frequencies.set_alpha(float(get_data[1]))
            # Frequencies.set_theta(float(get_data[2]))
            # Frequencies.set_alpha(float(get_data[3]))
            # Frequencies.set_beta(float(get_data[4]))
            # Frequencies.set_gamma(float(get_data[5]))
            # Channel1.append(0)
            # Channel2.append(0)
            # Channel3.append(0)
            # Channel4.append(0)
        else:
            Channel0.append(0)
            Channel1.append(0)
            Channel2.append(0)
            Channel3.append(0)
            Channel4.append(0)