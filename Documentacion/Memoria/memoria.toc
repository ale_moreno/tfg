\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivaci\IeC {\'o}n}{9}{section.1.1}
\contentsline {section}{\numberline {1.2}Estado del arte}{9}{section.1.2}
\contentsline {chapter}{\numberline {2}Conocimientos previos de Neurolog\IeC {\'\i }a}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}El cerebro humano}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Electroencefalograma}{12}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Posicionamiento de los electrodos}{13}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Configuraci\IeC {\'o}n y montaje de electrodos}{13}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Ondas cerebrales}{14}{subsection.2.2.3}
\contentsline {subsubsection}{Ondas Delta}{14}{section*.11}
\contentsline {subsubsection}{Ondas Theta}{15}{section*.13}
\contentsline {subsubsection}{Ondas Alpha}{15}{section*.15}
\contentsline {subsubsection}{Ondas Beta}{15}{section*.17}
\contentsline {subsubsection}{Ondas Gamma}{16}{section*.18}
\contentsline {section}{\numberline {2.3}Migra\IeC {\~n}a}{16}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Anomal\IeC {\'\i }as frecuentes}{17}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Memoria descriptiva}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Hardware}{18}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Sensores}{18}{subsection.3.1.1}
\contentsline {subsubsection}{OpenBCI EEG Headband Kit}{19}{section*.23}
\contentsline {subsubsection}{EMG/ECG Snap Electrode Cables}{20}{section*.25}
\contentsline {subsection}{\numberline {3.1.2}Placa de desarrollo}{20}{subsection.3.1.2}
\contentsline {subsubsection}{Teensy 3.6}{21}{section*.28}
\contentsline {subsection}{\numberline {3.1.3}Teensy 3 - Bluetooth LE 4.0 Module}{22}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Esquem\IeC {\'a}tico}{22}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Circuito amplificador}{24}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Simulaci\IeC {\'o}n}{25}{section.3.3}
\contentsline {section}{\numberline {3.4}Protoboard}{25}{section.3.4}
\contentsline {section}{\numberline {3.5}Osciloscopio virtual}{27}{section.3.5}
\contentsline {section}{\numberline {3.6}Transformada de Fourier}{28}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Short-Time Fourier Transform}{28}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Fast Fourier Transform}{30}{subsection.3.6.2}
\contentsline {section}{\numberline {3.7}Estudio con Matlab}{31}{section.3.7}
\contentsline {section}{\numberline {3.8}Aplicaci\IeC {\'o}n Python}{32}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Funcionamiento}{33}{subsection.3.8.1}
\contentsline {subsubsection}{Get\_data}{34}{section*.45}
\contentsline {subsubsection}{Frequencies}{35}{section*.48}
\contentsline {subsubsection}{Main}{36}{section*.50}
\contentsline {section}{\numberline {3.9}C\IeC {\'o}digo Teensy}{39}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}Variables}{40}{subsection.3.9.1}
\contentsline {subsection}{\numberline {3.9.2}Recolecci\IeC {\'o}n datos}{41}{subsection.3.9.2}
\contentsline {subsubsection}{Llamada a la funci\IeC {\'o}n}{41}{section*.58}
\contentsline {subsubsection}{C\IeC {\'a}lculos}{41}{section*.60}
\contentsline {subsubsection}{Elecci\IeC {\'o}n del canal}{42}{section*.62}
\contentsline {subsection}{\numberline {3.9.3}FFT}{43}{subsection.3.9.3}
\contentsline {subsection}{\numberline {3.9.4}Subfrecuencias}{43}{subsection.3.9.4}
\contentsline {chapter}{\numberline {4}Experimentaci\IeC {\'o}n}{44}{chapter.4}
\contentsline {section}{\numberline {4.1}Dise\IeC {\~n}o y construcci\IeC {\'o}n de placa}{44}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Kicad}{44}{subsection.4.1.1}
\contentsline {subsubsection}{Impresi\IeC {\'o}n}{45}{section*.68}
\contentsline {subsection}{\numberline {4.1.2}Error de impresi\IeC {\'o}n}{47}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Obtenci\IeC {\'o}n de datos}{48}{section.4.2}
\contentsline {section}{\numberline {4.3}Pruebas de ejercicios}{48}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Usuario 1}{48}{subsection.4.3.1}
\contentsline {subsubsection}{Hora de la medida: 11:46}{49}{section*.73}
\contentsline {subsubsection}{Hora de la medida: 11:48}{50}{section*.76}
\contentsline {subsection}{\numberline {4.3.2}Usuario 2}{51}{subsection.4.3.2}
\contentsline {subsubsection}{Hora de la medida: 12:08}{51}{section*.79}
\contentsline {subsubsection}{Hora de la medida: 12:10}{52}{section*.82}
\contentsline {section}{\numberline {4.4}M\IeC {\'u}sica estridente}{53}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Usuario 1}{53}{subsection.4.4.1}
\contentsline {subsubsection}{Hora de la medida: 11:51}{53}{section*.85}
\contentsline {subsubsection}{Hora de la medida: 11:53}{55}{section*.88}
\contentsline {subsubsection}{Hora de la medida: 11:55}{56}{section*.91}
\contentsline {subsection}{\numberline {4.4.2}Usuario 2}{57}{subsection.4.4.2}
\contentsline {subsubsection}{Hora de la medida: 12:13}{58}{section*.95}
\contentsline {subsubsection}{Hora de la medida: 12:14}{59}{section*.98}
\contentsline {subsubsection}{Hora de la medida: 12:16}{60}{section*.101}
\contentsline {section}{\numberline {4.5}M\IeC {\'u}sica relajante}{61}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Usuario 1}{61}{subsection.4.5.1}
\contentsline {subsubsection}{Hora de la medida: 11:58}{61}{section*.104}
\contentsline {subsubsection}{Hora de la medida: 11:59}{62}{section*.107}
\contentsline {subsubsection}{Hora de la medida: 12:01}{63}{section*.110}
\contentsline {subsection}{\numberline {4.5.2}Usuario 2}{64}{subsection.4.5.2}
\contentsline {subsubsection}{Hora de la medida: 12:17}{64}{section*.113}
\contentsline {subsubsection}{Hora de la medida: 12:19}{65}{section*.116}
\contentsline {subsubsection}{Hora de la medida: 12:20}{66}{section*.119}
\contentsline {section}{\numberline {4.6}Conclusiones}{67}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Prueba de ejercicio}{67}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}M\IeC {\'u}sica estridente}{68}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}M\IeC {\'u}sica relajante}{69}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}Conclusi\IeC {\'o}n}{70}{subsection.4.6.4}
\contentsline {chapter}{\numberline {5}Conclusiones}{71}{chapter.5}
\contentsline {section}{\numberline {5.1}Mejoras}{71}{section.5.1}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{72}{section.5.1}
\contentsline {chapter}{\numberline {A}C\IeC {\'o}digo Matlab}{85}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}C\IeC {\'a}lculo de frecuencias}{85}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Ejecuci\IeC {\'o}n tiempo real}{86}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Estudio de datos}{87}{section.Alph1.3}
\contentsline {section}{\numberline {A.4}FFT}{90}{section.Alph1.4}
\contentsline {section}{\numberline {A.5}Recolecci\IeC {\'o}n de datos}{91}{section.Alph1.5}
\contentsline {section}{\numberline {A.6}Recolecci\IeC {\'o}n de datos por minuto}{93}{section.Alph1.6}
\contentsline {section}{\numberline {A.7}Comprobaci\IeC {\'o}n del serial}{95}{section.Alph1.7}
\contentsline {chapter}{\numberline {B}C\IeC {\'o}digo en Python}{96}{appendix.Alph2}
\contentsline {section}{\numberline {B.1}Main.py}{96}{section.Alph2.1}
\contentsline {section}{\numberline {B.2}Get\_data.py}{100}{section.Alph2.2}
\contentsline {section}{\numberline {B.3}Frequencies}{101}{section.Alph2.3}
\contentsline {chapter}{\numberline {C}C\IeC {\'o}digo en Teensy}{103}{appendix.Alph3}
\contentsline {chapter}{\numberline {D}Reacondicionamiento}{108}{appendix.Alph4}
\contentsline {section}{\numberline {D.1}Filtro paso alto}{109}{section.Alph4.1}
\contentsline {section}{\numberline {D.2}Filtro paso bajo activo}{110}{section.Alph4.2}
\contentsline {section}{\numberline {D.3}Filtro paso bajo pasivo}{111}{section.Alph4.3}
\contentsline {section}{\numberline {D.4}Gr\IeC {\'a}fica filtro paso alto pasivo}{112}{section.Alph4.4}
\contentsline {section}{\numberline {D.5}Gr\IeC {\'a}fica filtro paso bajo activo}{112}{section.Alph4.5}
\contentsline {section}{\numberline {D.6}Gr\IeC {\'a}fica filtro paso bajo pasivo}{113}{section.Alph4.6}
\contentsline {section}{\numberline {D.7}Gr\IeC {\'a}fica filtro paso banda}{114}{section.Alph4.7}
