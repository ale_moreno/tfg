#include "arduinoFFT.h"

#define SAMPLES 512            
#define CHANNELS 5
#define SAMPLING_FREQUENCY 200 

arduinoFFT FFT = arduinoFFT();

double vReal[CHANNELS][SAMPLES];
double vImag[CHANNELS][SAMPLES];

float Channel0 = A1;
float Channel1 = A2;
float Channel2 = A3;
float Channel3 = A4;
float Channel4 = A5;

int val = 0;
float measureT = 0.0;

float Delta[CHANNELS];
float Theta[CHANNELS];
float Alpha[CHANNELS];
float Beta[CHANNELS];
float Gamma[CHANNELS];
float total[CHANNELS];

float _Delta = 0.0;
float _Theta = 0.0;
float _Alpha = 0.0;
float _Beta = 0.0;
float _Gamma = 0.0;

float freq = 0.0;


float MeasureT(int msInterval, float(*funct)(int n), int chan)
{
   float sum=0.0;
   long startTime = millis();
   int samplesNumber = 0;
 
   while (millis() - startTime < msInterval)
   {
      samplesNumber++;
      sum += funct(chan);
   }

   return sum / samplesNumber;
}
 
float getMeasure(int chan)
{
//   return val = analogRead(ANALOG_IN);
  switch(chan){
    case 0:
      val = analogRead(Channel0);
      break;
    case 1:
      val = analogRead(Channel1);
     break;
    case 2:
      val = analogRead(Channel2);
      break;
    case 3:
      val = analogRead(Channel3);
      break;
    case 4:
      val = analogRead(Channel4);
      break;
  }
  return val;
}

void _fft(int chan){
    FFT.Windowing(vReal[chan], SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
    FFT.Compute(vReal[chan], vImag[chan], SAMPLES, FFT_FORWARD);
    FFT.ComplexToMagnitude(vReal[chan], vImag[chan], SAMPLES);
}

void subfrequencies(int chan){

      for(int i=0; i<(SAMPLES/2); i++)
    {
        total[chan] += vReal[chan][i];
        freq = (i * 0.5 * SAMPLING_FREQUENCY) / SAMPLES, 1;
        if(freq >= 0.5 && freq <= 3.5)
            Delta[chan] =+ vReal[chan][i];
        else if(freq > 3.5 && freq <= 7.5)
            Theta[chan] =+ vReal[chan][i];
        else if(freq > 7.5 && freq <= 13)
            Alpha[chan] =+ vReal[chan][i];
        else if(freq > 13 && freq <= 30) 
            Beta[chan] =+ vReal[chan][i];
        else if(freq > 30 && freq <= 60)
            Gamma[chan] =+ vReal[chan][i];
    }
      Delta[chan] /= total[chan];
      Theta[chan] /= total[chan];
      Alpha[chan] /= total[chan];
      Beta[chan] /= total[chan];
      Gamma[chan] /= total[chan];
}

void median(){

  for(int i = 0; i<CHANNELS; i++){
    _Delta += Delta[i];
    _Theta += Theta[i];
    _Alpha += Alpha [i];
    _Beta += Beta [i];
    _Gamma +=  Gamma [i];
  }
  _Delta /= 5;
  _Theta /= 5;
  _Alpha /= 5;
  _Beta /= 5;
  _Gamma /= 5;
}

void frequencies(){
  for(int i=0; i<SAMPLES; i++)
    {
      for(int j=0;j < CHANNELS; j++){
          vReal[j][i] = MeasureT(80,getMeasure, j);
          vImag[j][i] = 0;
      }

          Serial.println(
            String(vReal[0][i],3) + "," +
            String(vReal[1][i],3) + "," +
            String(vReal[2][i],3) + "," +
            String(vReal[3][i],3) + "," + 
            String(vReal[4][i],3) + "," + 
            String(_Delta,10) + "," + 
            String(_Theta,10) + "," + 
            String(_Alpha,10) + "," + 
            String(_Beta,10) + "," + 
            String(_Gamma,10)
          );
    }

    for(int i = 0; i< CHANNELS; i++){
      _fft(i);
      subfrequencies(i);
    }

    median();
}

void setup()
{
    Serial.begin(57600);
}
 
void loop()
{
	// Reset all variables
	_Delta = 0.0;
    _Theta = 0.0;
    _Alpha = 0.0;
    _Beta = 0.0;
    _Gamma =  0.0;

    for(int i = 0; i< CHANNELS; i++){
      total[i]=0.0; Delta[i]=0.0; Theta[i]=0.0; Alpha[i]=0.0; Beta[i]=0.0; Gamma[i]=0.0;
    }
  frequencies();
}
