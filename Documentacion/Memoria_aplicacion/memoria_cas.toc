\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Aplicaci\IeC {\'o}n Python}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Funcionamiento}{6}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Get\_data}{6}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Frequencies}{8}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Main}{9}{subsection.1.1.3}
\contentsline {chapter}{\numberline {2}C\IeC {\'o}digo Teensy}{12}{chapter.2}
\contentsline {section}{\numberline {2.1}Variables}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Recolecci\IeC {\'o}n datos}{14}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Llamada a la funci\IeC {\'o}n}{14}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}C\IeC {\'a}lculos}{15}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Elecci\IeC {\'o}n del canal}{15}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}FFT}{16}{section.2.3}
\contentsline {section}{\numberline {2.4}Subfrecuencias}{17}{section.2.4}
\contentsline {chapter}{\numberline {A}C\IeC {\'o}digo en Python}{19}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Main.py}{19}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Get\_data.py}{23}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Frequencies}{24}{section.Alph1.3}
\contentsline {chapter}{\numberline {B}C\IeC {\'o}digo en C}{27}{appendix.Alph2}
