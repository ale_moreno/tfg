\beamer@sectionintoc {1}{Introducci\IeC {\'o}n}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Estado del arte}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Neurolog\IeC {\'\i }a}{5}{0}{1}
\beamer@sectionintoc {2}{Memoria descriptiva}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Componentes}{9}{0}{2}
\beamer@subsectionintoc {2}{2}{Esquem\IeC {\'a}tico}{11}{0}{2}
\beamer@subsectionintoc {2}{3}{PCB}{12}{0}{2}
\beamer@subsectionintoc {2}{4}{Protoboard}{15}{0}{2}
\beamer@subsectionintoc {2}{5}{Transformada de Fourier}{17}{0}{2}
\beamer@subsectionintoc {2}{6}{Codificaci\IeC {\'o}n}{18}{0}{2}
\beamer@subsubsectionintoc {2}{6}{1}{C\IeC {\'o}digo Teensy}{19}{0}{2}
\beamer@subsubsectionintoc {2}{6}{2}{Aplicaci\IeC {\'o}n Python}{20}{0}{2}
\beamer@sectionintoc {3}{Experimentaci\IeC {\'o}n}{22}{0}{3}
\beamer@subsectionintoc {3}{1}{Prueba de ejercicios matem\IeC {\'a}ticos}{22}{0}{3}
\beamer@subsectionintoc {3}{2}{M\IeC {\'u}sica estridente}{23}{0}{3}
\beamer@subsectionintoc {3}{3}{M\IeC {\'u}sica relajante}{24}{0}{3}
\beamer@subsectionintoc {3}{4}{Comparaciones}{25}{0}{3}
