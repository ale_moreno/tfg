EESchema Schematic File Version 4
LIBS:Notch-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5C00520C
P 3650 2450
F 0 "R1" H 3720 2496 50  0000 L CNN
F 1 "R" H 3720 2405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3580 2450 50  0001 C CNN
F 3 "~" H 3650 2450 50  0001 C CNN
	1    3650 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C0052A3
P 3650 3100
F 0 "R2" H 3720 3146 50  0000 L CNN
F 1 "R" H 3720 3055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3580 3100 50  0001 C CNN
F 3 "~" H 3650 3100 50  0001 C CNN
	1    3650 3100
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:CA3140 U1
U 1 1 5C00538D
P 4500 2750
F 0 "U1" H 4841 2796 50  0000 L CNN
F 1 "CA3140" H 4841 2705 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W10.16mm" H 4400 2650 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/ca31/ca3140-a.pdf" H 4500 2750 50  0001 C CNN
	1    4500 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2600 3650 2650
Wire Wire Line
	3650 3250 3650 3350
Wire Wire Line
	3650 2300 3650 2250
Wire Wire Line
	4400 2450 4400 2250
Wire Wire Line
	4400 2250 3650 2250
Connection ~ 3650 2250
Wire Wire Line
	3650 2250 3650 2200
Wire Wire Line
	4200 2850 4200 3150
Wire Wire Line
	4200 3150 4800 3150
Wire Wire Line
	4800 3150 4800 2750
Wire Wire Line
	4800 2750 5250 2750
Connection ~ 4800 2750
Wire Wire Line
	3650 2650 4200 2650
Connection ~ 3650 2650
Wire Wire Line
	3650 2650 3650 2950
Wire Wire Line
	4400 3050 4400 3350
Wire Wire Line
	4400 3350 3650 3350
Connection ~ 3650 3350
Wire Wire Line
	3650 3350 3650 3400
$Comp
L Connector_Generic:Conn_01x01 JVCC1
U 1 1 5C0057BC
P 3650 2000
F 0 "JVCC1" V 3616 1912 50  0000 R CNN
F 1 "Conn_01x01" V 3525 1912 50  0000 R CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 3650 2000 50  0001 C CNN
F 3 "~" H 3650 2000 50  0001 C CNN
	1    3650 2000
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 JGND1
U 1 1 5C0057FC
P 3650 3600
F 0 "JGND1" V 3523 3680 50  0000 L CNN
F 1 "Conn_01x01" V 3614 3680 50  0000 L CNN
F 2 "Connector_Pin:Pin_D1.0mm_L10.0mm" H 3650 3600 50  0001 C CNN
F 3 "~" H 3650 3600 50  0001 C CNN
	1    3650 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 2950 5250 2850
Connection ~ 5250 2750
Wire Wire Line
	5250 3150 5250 3050
Connection ~ 5250 2950
Wire Wire Line
	5250 3150 5250 3250
Connection ~ 5250 3150
$Comp
L Connector:Conn_01x06_Male J3
U 1 1 5C08FC6C
P 5850 2950
F 0 "J3" H 5823 2923 50  0000 R CNN
F 1 "Conn_01x06_Male" H 5823 2832 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x06_P1.27mm_Vertical" H 5850 2950 50  0001 C CNN
F 3 "~" H 5850 2950 50  0001 C CNN
	1    5850 2950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5250 2750 5650 2750
Wire Wire Line
	5650 2850 5250 2850
Connection ~ 5250 2850
Wire Wire Line
	5250 2850 5250 2750
Wire Wire Line
	5250 2950 5650 2950
Wire Wire Line
	5650 3050 5250 3050
Connection ~ 5250 3050
Wire Wire Line
	5250 3050 5250 2950
Wire Wire Line
	5250 3150 5650 3150
Wire Wire Line
	5650 3250 5250 3250
$EndSCHEMATC
