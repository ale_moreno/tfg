EESchema Schematic File Version 4
LIBS:Migrainedetector-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Instrumentation:AD620 U1
U 1 1 5B9B84E1
P 3850 3700
F 0 "U1" H 4291 3746 50  0000 L CNN
F 1 "AD620" H 4291 3655 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3850 3700 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/AD620.pdf" H 3850 3700 50  0001 C CNN
	1    3850 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3900 3450 3900
$Comp
L Amplifier_Operational:CA3130 U2
U 1 1 5B9B88BC
P 6100 3800
F 0 "U2" H 6300 3700 50  0000 L CNN
F 1 "3130" H 6300 3950 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 6000 3700 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/ca31/ca3130-a.pdf" H 6100 3800 50  0001 C CNN
	1    6100 3800
	1    0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5B9BC83A
P 3250 3700
F 0 "R1" H 3320 3746 50  0000 L CNN
F 1 "2.2K" V 3150 3600 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3180 3700 50  0001 C CNN
F 3 "~" H 3250 3700 50  0001 C CNN
	1    3250 3700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5B9C059E
P 6000 5400
F 0 "R3" V 5793 5400 50  0000 C CNN
F 1 "1M" V 5884 5400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5930 5400 50  0001 C CNN
F 3 "~" H 6000 5400 50  0001 C CNN
	1    6000 5400
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5B9C1927
P 6000 4800
F 0 "R4" V 5793 4800 50  0000 C CNN
F 1 "1M" V 5884 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5930 4800 50  0001 C CNN
F 3 "~" H 6000 4800 50  0001 C CNN
	1    6000 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 4800 6150 4800
$Comp
L Device:C C2
U 1 1 5B9C1EAA
P 6000 5000
F 0 "C2" V 6050 5200 50  0000 C CNN
F 1 ".0068" V 5950 5200 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 6038 4850 50  0001 C CNN
F 3 "~" H 6000 5000 50  0001 C CNN
	1    6000 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 3550 3450 3550
Wire Wire Line
	3450 3550 3450 3600
Wire Wire Line
	3250 3850 3450 3850
Wire Wire Line
	3450 3850 3450 3800
Text Label 2850 3500 0    50   ~ 0
Active_electrode
Text Label 2850 3950 0    50   ~ 0
Active_electrode-
$Comp
L Device:R_POT_US RV1
U 1 1 5B9C07BC
P 5550 5400
F 0 "RV1" H 5482 5446 50  0000 R CNN
F 1 "1M" H 5482 5355 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 5550 5400 50  0001 C CNN
F 3 "~" H 5550 5400 50  0001 C CNN
	1    5550 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5C00082E
P 4650 3700
F 0 "C1" V 4398 3700 50  0000 C CNN
F 1 "1uF" V 4489 3700 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 4688 3550 50  0001 C CNN
F 3 "~" H 4650 3700 50  0001 C CNN
	1    4650 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5C0008C6
P 5200 3700
F 0 "R2" V 4993 3700 50  0000 C CNN
F 1 "113K" V 5084 3700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5130 3700 50  0001 C CNN
F 3 "~" H 5200 3700 50  0001 C CNN
	1    5200 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	4250 3700 4500 3700
Wire Wire Line
	4800 3700 5050 3700
Wire Wire Line
	2400 3500 3450 3500
Wire Wire Line
	2400 3600 2850 3600
Wire Wire Line
	2850 3600 2850 3900
Wire Wire Line
	5350 3700 5450 3700
Wire Wire Line
	6150 5000 6750 5000
Wire Wire Line
	6750 5000 6750 4800
Wire Wire Line
	6750 4800 6750 3800
Wire Wire Line
	6750 3800 6400 3800
Connection ~ 6750 4800
Wire Wire Line
	5850 5000 5550 5000
Wire Wire Line
	5550 5000 5550 4800
Connection ~ 5550 3700
Wire Wire Line
	5550 3700 5800 3700
Wire Wire Line
	5850 4800 5550 4800
Connection ~ 5550 4800
Wire Wire Line
	5550 4800 5550 3700
Wire Wire Line
	5700 5400 5850 5400
Wire Wire Line
	5550 5250 5550 5000
Connection ~ 5550 5000
$Comp
L Device:R R5
U 1 1 5C005F9C
P 7100 3800
F 0 "R5" V 6893 3800 50  0000 C CNN
F 1 "10K" V 6984 3800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7030 3800 50  0001 C CNN
F 3 "~" H 7100 3800 50  0001 C CNN
	1    7100 3800
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5C005FEF
P 7550 3250
F 0 "C3" V 7600 3450 50  0000 C CNN
F 1 ".0068" V 7500 3450 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 7588 3100 50  0001 C CNN
F 3 "~" H 7550 3250 50  0001 C CNN
	1    7550 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6950 3800 6750 3800
Connection ~ 6750 3800
Wire Wire Line
	7250 3800 7550 3800
Wire Wire Line
	7550 3800 7550 3400
Wire Wire Line
	5300 4300 5450 4300
Wire Wire Line
	5450 4300 5450 3700
Connection ~ 5450 3700
Wire Wire Line
	5450 3700 5550 3700
Wire Wire Line
	2400 3700 2750 3700
Wire Wire Line
	2750 3700 2750 4500
Wire Wire Line
	2750 4500 5300 4500
Connection ~ 5300 4500
Wire Wire Line
	5300 4500 5300 4300
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5C007F1A
P 8250 3800
F 0 "J2" H 8329 3842 50  0000 L CNN
F 1 "V_out" H 8329 3751 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 8250 3800 50  0001 C CNN
F 3 "~" H 8250 3800 50  0001 C CNN
	1    8250 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3800 8050 3800
Connection ~ 7550 3800
NoConn ~ 3950 4000
NoConn ~ 6100 4100
NoConn ~ 6200 4100
NoConn ~ 6000 3500
NoConn ~ 6100 3500
NoConn ~ 5550 5550
Wire Wire Line
	5350 2700 5150 2700
Wire Wire Line
	3750 2700 3750 3400
Wire Wire Line
	5450 2700 5450 2900
Wire Wire Line
	5450 3300 4650 3300
Wire Wire Line
	4650 3300 4650 4100
Wire Wire Line
	4650 4100 3750 4100
Wire Wire Line
	3750 4100 3750 4000
Wire Wire Line
	5450 3300 6000 3300
Wire Wire Line
	6000 3300 6000 3500
Connection ~ 5450 3300
Wire Wire Line
	5150 3100 6550 3100
Wire Wire Line
	6550 3100 6550 4350
Wire Wire Line
	6550 4350 6000 4350
Wire Wire Line
	6000 4350 6000 4100
Wire Wire Line
	5150 2700 5150 3100
Connection ~ 5150 2700
Wire Wire Line
	5150 2700 3750 2700
Wire Wire Line
	6550 4350 6550 5400
Wire Wire Line
	6150 5400 6550 5400
Connection ~ 6550 4350
Wire Wire Line
	5450 2900 7550 2900
Wire Wire Line
	7550 2900 7550 3100
Connection ~ 5450 2900
Wire Wire Line
	5450 2900 5450 3300
Wire Wire Line
	5550 2700 5550 2800
Wire Wire Line
	5550 2800 5700 2800
Wire Wire Line
	5700 2800 5700 3900
Wire Wire Line
	5700 3900 5800 3900
Wire Wire Line
	5700 3900 5700 4700
Wire Wire Line
	5700 4700 5300 4700
Wire Wire Line
	5300 4500 5300 4700
Connection ~ 5700 3900
Text Label 4900 2700 0    50   ~ 0
v-5+
Text Label 5450 2950 1    50   ~ 0
Earth
Text Label 5600 2800 0    50   ~ 0
Virtual_Earth
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 5C09008C
P 5350 2500
F 0 "J4" H 5250 2500 50  0000 L CNN
F 1 "01" H 5350 2450 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5350 2500 50  0001 C CNN
F 3 "~" H 5350 2500 50  0001 C CNN
	1    5350 2500
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J5
U 1 1 5C09205F
P 5450 2500
F 0 "J5" H 5350 2500 50  0000 L CNN
F 1 "01" H 5450 2450 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5450 2500 50  0001 C CNN
F 3 "~" H 5450 2500 50  0001 C CNN
	1    5450 2500
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5C0920C7
P 5550 2500
F 0 "J3" H 5450 2500 50  0000 L CNN
F 1 "01" H 5550 2450 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5550 2500 50  0001 C CNN
F 3 "~" H 5550 2500 50  0001 C CNN
	1    5550 2500
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J1
U 1 1 5C0948A4
P 2200 3500
F 0 "J1" H 2100 3500 50  0000 L CNN
F 1 "01" H 2400 3600 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 2200 3500 50  0001 C CNN
F 3 "~" H 2200 3500 50  0001 C CNN
	1    2200 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J6
U 1 1 5C0948FC
P 2200 3600
F 0 "J6" H 2100 3600 50  0000 L CNN
F 1 "01" H 2400 3650 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 2200 3600 50  0001 C CNN
F 3 "~" H 2200 3600 50  0001 C CNN
	1    2200 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J7
U 1 1 5C094963
P 2200 3700
F 0 "J7" H 2100 3700 50  0000 L CNN
F 1 "01" H 2400 3750 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 2200 3700 50  0001 C CNN
F 3 "~" H 2200 3700 50  0001 C CNN
	1    2200 3700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
