EESchema Schematic File Version 4
LIBS:PCB-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Instrumentation:AD620 U1
U 1 1 5B9B84E1
P 2350 950
F 0 "U1" H 2300 1000 50  0000 L CNN
F 1 "AD620" H 2200 900 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2350 950 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/AD620.pdf" H 2350 950 50  0001 C CNN
	1    2350 950 
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:CA3130 U2
U 1 1 5B9B88BC
P 3900 1050
F 0 "U2" H 4100 950 50  0000 L CNN
F 1 "3130" H 4100 1200 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3800 950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/ca31/ca3130-a.pdf" H 3900 1050 50  0001 C CNN
	1    3900 1050
	1    0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5B9BC83A
P 1750 950
F 0 "R1" V 1750 900 39  0000 L CNN
F 1 "2.2K" V 1650 850 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1680 950 50  0001 C CNN
F 3 "~" H 1750 950 50  0001 C CNN
	1    1750 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 5B9C059E
P 3700 1850
F 0 "R4" V 3800 1850 50  0000 C CNN
F 1 "1M" V 3850 1850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3630 1850 50  0001 C CNN
F 3 "~" H 3700 1850 50  0001 C CNN
	1    3700 1850
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5B9C1927
P 3700 1550
F 0 "R3" V 3700 1500 50  0000 C CNN
F 1 "1M" V 3584 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3630 1550 50  0001 C CNN
F 3 "~" H 3700 1550 50  0001 C CNN
	1    3700 1550
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5B9C1EAA
P 3700 1700
F 0 "C2" V 3750 1700 50  0000 C CNN
F 1 ".0068" V 3650 1900 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 3738 1550 50  0001 C CNN
F 3 "~" H 3700 1700 50  0001 C CNN
	1    3700 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 800  1950 800 
Wire Wire Line
	1950 800  1950 850 
Wire Wire Line
	1750 1100 1950 1100
Wire Wire Line
	1950 1100 1950 1050
$Comp
L Device:R_POT_US RV1
U 1 1 5B9C07BC
P 3400 1850
F 0 "RV1" H 3332 1896 50  0000 R CNN
F 1 "1M" H 3332 1805 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 3400 1850 50  0001 C CNN
F 3 "~" H 3400 1850 50  0001 C CNN
	1    3400 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5C00082E
P 2900 950
F 0 "C1" V 2648 950 50  0000 C CNN
F 1 "1uF" V 2739 950 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 2938 800 50  0001 C CNN
F 3 "~" H 2900 950 50  0001 C CNN
	1    2900 950 
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5C0008C6
P 3200 950
F 0 "R2" V 2993 950 50  0000 C CNN
F 1 "113K" V 3084 950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3130 950 50  0001 C CNN
F 3 "~" H 3200 950 50  0001 C CNN
	1    3200 950 
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5C005F9C
P 4400 1050
F 0 "R5" V 4193 1050 50  0000 C CNN
F 1 "10K" V 4284 1050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4330 1050 50  0001 C CNN
F 3 "~" H 4400 1050 50  0001 C CNN
	1    4400 1050
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5C005FEF
P 4700 1250
F 0 "C3" V 4750 1450 50  0000 C CNN
F 1 ".68" V 4650 1450 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 4738 1100 50  0001 C CNN
F 3 "~" H 4700 1250 50  0001 C CNN
	1    4700 1250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 5C007F1A
P 5050 1050
F 0 "J6" H 5129 1092 50  0000 L CNN
F 1 "V_out" H 5129 1001 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5050 1050 50  0001 C CNN
F 3 "~" H 5050 1050 50  0001 C CNN
	1    5050 1050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J1
U 1 1 5C0948A4
P 900 850
F 0 "J1" H 800 850 50  0000 L CNN
F 1 "A_electrode_+" H 1100 900 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 900 850 50  0001 C CNN
F 3 "~" H 900 850 50  0001 C CNN
	1    900  850 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 5C0948FC
P 900 950
F 0 "J2" H 800 950 50  0000 L CNN
F 1 "A_electrode_-" H 1100 1000 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 900 950 50  0001 C CNN
F 3 "~" H 900 950 50  0001 C CNN
	1    900  950 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5C094963
P 900 1050
F 0 "J3" H 800 1050 50  0000 L CNN
F 1 "N_electrode" H 1100 1100 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 900 1050 50  0001 C CNN
F 3 "~" H 900 1050 50  0001 C CNN
	1    900  1050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5C00520C
P 7300 1350
F 0 "R7" H 7370 1396 50  0000 L CNN
F 1 "R" H 7370 1305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7230 1350 50  0001 C CNN
F 3 "~" H 7300 1350 50  0001 C CNN
	1    7300 1350
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5C0052A3
P 7300 1750
F 0 "R6" H 7370 1796 50  0000 L CNN
F 1 "R" H 7370 1705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7230 1750 50  0001 C CNN
F 3 "~" H 7300 1750 50  0001 C CNN
	1    7300 1750
	-1   0    0    -1  
$EndComp
NoConn ~ 6800 1350
NoConn ~ 6700 1350
NoConn ~ 6800 1950
Text Label 6300 1650 2    50   ~ 0
Virtual_GND
$Comp
L Connector:Conn_01x01_Male J5
U 1 1 5C09205F
P 8850 2750
F 0 "J5" H 8750 2750 50  0000 L CNN
F 1 "GND" H 8850 2850 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 8850 2750 50  0001 C CNN
F 3 "~" H 8850 2750 50  0001 C CNN
	1    8850 2750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 5C09008C
P 8850 2250
F 0 "J4" H 8750 2250 50  0000 L CNN
F 1 "VCC" H 8850 2300 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 8850 2250 50  0001 C CNN
F 3 "~" H 8850 2250 50  0001 C CNN
	1    8850 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1100 850  1650 850 
Wire Wire Line
	1650 850  1650 750 
Wire Wire Line
	1650 750  1950 750 
Wire Wire Line
	1100 950  1650 950 
Wire Wire Line
	1650 950  1650 1150
Wire Wire Line
	1650 1150 1950 1150
NoConn ~ 2450 1250
Wire Wire Line
	3550 1700 3400 1700
Wire Wire Line
	3400 1700 3400 1550
Wire Wire Line
	3400 950  3350 950 
Connection ~ 3400 1700
Wire Wire Line
	3400 950  3600 950 
Connection ~ 3400 950 
Wire Wire Line
	3550 1550 3400 1550
Connection ~ 3400 1550
Wire Wire Line
	3400 1550 3400 950 
Wire Wire Line
	3850 1700 4250 1700
Wire Wire Line
	4250 1700 4250 1550
Wire Wire Line
	4250 1050 4200 1050
Wire Wire Line
	3850 1550 4250 1550
Connection ~ 4250 1550
Wire Wire Line
	4250 1550 4250 1050
Connection ~ 4250 1050
Wire Wire Line
	4550 1050 4700 1050
Wire Wire Line
	4700 1100 4700 1050
Connection ~ 4700 1050
Wire Wire Line
	4700 1050 4850 1050
Wire Wire Line
	4700 1400 5150 1400
Wire Wire Line
	5150 1400 5150 750 
Wire Wire Line
	3850 1850 4850 1850
NoConn ~ 3400 2000
NoConn ~ 3900 1350
NoConn ~ 4000 1350
NoConn ~ 3900 750 
Wire Wire Line
	3800 750  3800 700 
Wire Wire Line
	3800 700  5150 700 
Wire Wire Line
	3800 1350 3800 1400
Wire Wire Line
	3800 1400 4600 1400
Wire Wire Line
	4600 1400 4600 800 
Wire Wire Line
	4600 800  4850 800 
Connection ~ 4850 800 
Wire Wire Line
	4850 800  4850 1850
$Comp
L Amplifier_Operational:CA3140 U3
U 1 1 5C00538D
P 6800 1650
F 0 "U3" H 7141 1696 50  0000 L CNN
F 1 "CA3140" H 7141 1605 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 6700 1550 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/ca31/ca3140-a.pdf" H 6800 1650 50  0001 C CNN
	1    6800 1650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7100 1750 7100 2000
Wire Wire Line
	7100 2000 6500 2000
Wire Wire Line
	6500 2000 6500 1650
Connection ~ 6500 1650
Wire Wire Line
	7300 1500 7300 1550
Wire Wire Line
	7100 1550 7300 1550
Connection ~ 7300 1550
Wire Wire Line
	7300 1550 7300 1600
Wire Wire Line
	6900 1350 6900 1200
Wire Wire Line
	6900 1200 7300 1200
Wire Wire Line
	6900 1950 7300 1950
Wire Wire Line
	7300 1950 7300 1900
Wire Wire Line
	5150 750  7450 750 
Wire Wire Line
	7450 750  7450 2000
Connection ~ 5150 750 
Wire Wire Line
	5150 750  5150 700 
Wire Wire Line
	7300 1950 7300 2000
Wire Wire Line
	7300 2000 7450 2000
Connection ~ 7300 1950
Wire Wire Line
	4850 650  7300 650 
Wire Wire Line
	7300 650  7300 1100
Connection ~ 4850 650 
Wire Wire Line
	4850 650  4850 800 
Connection ~ 7300 1200
Wire Wire Line
	1100 1050 1600 1050
Wire Wire Line
	1600 1350 3300 1350
Wire Wire Line
	3300 1350 3300 1150
Wire Wire Line
	3300 1150 3600 1150
Wire Wire Line
	1600 1050 1600 1350
Wire Wire Line
	4900 1650 4900 2050
Wire Wire Line
	4900 1650 5850 1650
Connection ~ 3300 1350
Wire Wire Line
	2250 650  4850 650 
Wire Wire Line
	2950 1250 2950 700 
Wire Wire Line
	2950 700  3800 700 
Connection ~ 3800 700 
Wire Wire Line
	3300 2050 4900 2050
Wire Wire Line
	3300 2050 3300 1350
$Comp
L Amplifier_Instrumentation:AD620 U1_2
U 1 1 5C0FA20D
P 2350 2550
F 0 "U1_2" H 2300 2600 50  0000 L CNN
F 1 "AD620" H 2200 2500 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2350 2550 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/AD620.pdf" H 2350 2550 50  0001 C CNN
	1    2350 2550
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:CA3130 U2_2
U 1 1 5C0FA213
P 3900 2650
F 0 "U2_2" H 3750 2650 50  0000 L CNN
F 1 "3130" H 4100 2800 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3800 2550 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/ca31/ca3130-a.pdf" H 3900 2650 50  0001 C CNN
	1    3900 2650
	1    0    0    1   
$EndComp
$Comp
L Device:R R1_2
U 1 1 5C0FA219
P 1750 2550
F 0 "R1_2" V 1750 2500 39  0000 L CNN
F 1 "2.2K" V 1650 2450 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1680 2550 50  0001 C CNN
F 3 "~" H 1750 2550 50  0001 C CNN
	1    1750 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4_2
U 1 1 5C0FA21F
P 3700 3450
F 0 "R4_2" V 3700 3450 50  0000 C CNN
F 1 "1M" V 3850 3450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3630 3450 50  0001 C CNN
F 3 "~" H 3700 3450 50  0001 C CNN
	1    3700 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R3_2
U 1 1 5C0FA225
P 3700 3150
F 0 "R3_2" V 3700 3100 50  0000 C CNN
F 1 "1M" V 3584 3150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3630 3150 50  0001 C CNN
F 3 "~" H 3700 3150 50  0001 C CNN
	1    3700 3150
	0    1    1    0   
$EndComp
$Comp
L Device:C C2_2
U 1 1 5C0FA22B
P 3700 3300
F 0 "C2_2" V 3750 3300 50  0000 C CNN
F 1 ".0068" V 3650 3500 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 3738 3150 50  0001 C CNN
F 3 "~" H 3700 3300 50  0001 C CNN
	1    3700 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 2400 1950 2400
Wire Wire Line
	1950 2400 1950 2450
Wire Wire Line
	1750 2700 1950 2700
Wire Wire Line
	1950 2700 1950 2650
$Comp
L Device:R_POT_US RV1_2
U 1 1 5C0FA235
P 3400 3450
F 0 "RV1_2" H 3332 3496 50  0000 R CNN
F 1 "1M" H 3332 3405 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 3400 3450 50  0001 C CNN
F 3 "~" H 3400 3450 50  0001 C CNN
	1    3400 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1_2
U 1 1 5C0FA23B
P 2900 2550
F 0 "C1_2" V 2648 2550 50  0000 C CNN
F 1 "1uF" V 2739 2550 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 2938 2400 50  0001 C CNN
F 3 "~" H 2900 2550 50  0001 C CNN
	1    2900 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R2_2
U 1 1 5C0FA241
P 3200 2550
F 0 "R2_2" V 3200 2550 50  0000 C CNN
F 1 "113K" V 3084 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3130 2550 50  0001 C CNN
F 3 "~" H 3200 2550 50  0001 C CNN
	1    3200 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R5_2
U 1 1 5C0FA247
P 4400 2650
F 0 "R5_2" V 4400 2650 50  0000 C CNN
F 1 "10K" V 4284 2650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4330 2650 50  0001 C CNN
F 3 "~" H 4400 2650 50  0001 C CNN
	1    4400 2650
	0    1    1    0   
$EndComp
$Comp
L Device:C C3_2
U 1 1 5C0FA24D
P 4700 2850
F 0 "C3_2" V 4750 3050 50  0000 C CNN
F 1 ".0068" V 4650 3050 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 4738 2700 50  0001 C CNN
F 3 "~" H 4700 2850 50  0001 C CNN
	1    4700 2850
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6_2
U 1 1 5C0FA253
P 5050 2650
F 0 "J6_2" H 5129 2692 50  0000 L CNN
F 1 "V_out" H 5129 2601 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5050 2650 50  0001 C CNN
F 3 "~" H 5050 2650 50  0001 C CNN
	1    5050 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J1_2
U 1 1 5C0FA259
P 900 2450
F 0 "J1_2" H 800 2450 50  0000 L CNN
F 1 "A_electrode_+" H 1100 2500 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 900 2450 50  0001 C CNN
F 3 "~" H 900 2450 50  0001 C CNN
	1    900  2450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J2_2
U 1 1 5C0FA25F
P 900 2550
F 0 "J2_2" H 800 2550 50  0000 L CNN
F 1 "A_electrode_-" H 1100 2600 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 900 2550 50  0001 C CNN
F 3 "~" H 900 2550 50  0001 C CNN
	1    900  2550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J3_2
U 1 1 5C0FA265
P 900 2650
F 0 "J3_2" H 800 2650 50  0000 L CNN
F 1 "N_electrode" H 1100 2700 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 900 2650 50  0001 C CNN
F 3 "~" H 900 2650 50  0001 C CNN
	1    900  2650
	1    0    0    -1  
$EndComp
Text Label 5650 3200 2    50   ~ 0
Virtual_GND
Wire Wire Line
	1100 2450 1650 2450
Wire Wire Line
	1650 2450 1650 2350
Wire Wire Line
	1650 2350 1950 2350
Wire Wire Line
	1100 2550 1650 2550
Wire Wire Line
	1650 2550 1650 2750
Wire Wire Line
	1650 2750 1950 2750
NoConn ~ 2450 2850
Wire Wire Line
	3550 3300 3400 3300
Wire Wire Line
	3400 3300 3400 3150
Wire Wire Line
	3400 2550 3350 2550
Connection ~ 3400 3300
Wire Wire Line
	3400 2550 3600 2550
Connection ~ 3400 2550
Wire Wire Line
	3550 3150 3400 3150
Connection ~ 3400 3150
Wire Wire Line
	3400 3150 3400 2550
Wire Wire Line
	3850 3300 4250 3300
Wire Wire Line
	4250 3300 4250 3150
Wire Wire Line
	4250 2650 4200 2650
Wire Wire Line
	3850 3150 4250 3150
Connection ~ 4250 3150
Wire Wire Line
	4250 3150 4250 2650
Connection ~ 4250 2650
Wire Wire Line
	4550 2650 4700 2650
Wire Wire Line
	4700 2700 4700 2650
Connection ~ 4700 2650
Wire Wire Line
	4700 2650 4850 2650
NoConn ~ 3400 3600
NoConn ~ 3900 2950
NoConn ~ 4000 2950
NoConn ~ 3900 2350
Wire Wire Line
	1100 2650 1600 2650
Wire Wire Line
	1600 2950 3300 2950
Wire Wire Line
	3300 2950 3300 2750
Wire Wire Line
	3300 2750 3600 2750
Wire Wire Line
	1600 2650 1600 2950
Wire Wire Line
	4900 3250 4900 3650
Connection ~ 3300 2950
Wire Wire Line
	3300 3650 4900 3650
Wire Wire Line
	3300 3650 3300 2950
Wire Wire Line
	2250 1250 2950 1250
Connection ~ 5850 1650
Wire Wire Line
	5850 1650 6500 1650
$Comp
L Amplifier_Instrumentation:AD620 U1_3
U 1 1 5C105069
P 2300 4100
F 0 "U1_3" H 2250 4150 50  0000 L CNN
F 1 "AD620" H 2150 4050 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2300 4100 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/AD620.pdf" H 2300 4100 50  0001 C CNN
	1    2300 4100
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:CA3130 U2_3
U 1 1 5C10506F
P 3850 4200
F 0 "U2_3" H 4050 4100 50  0000 L CNN
F 1 "3130" H 4050 4350 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3750 4100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/ca31/ca3130-a.pdf" H 3850 4200 50  0001 C CNN
	1    3850 4200
	1    0    0    1   
$EndComp
$Comp
L Device:R R1_3
U 1 1 5C105075
P 1700 4100
F 0 "R1_3" V 1700 4050 39  0000 L CNN
F 1 "2.2K" V 1600 4000 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1630 4100 50  0001 C CNN
F 3 "~" H 1700 4100 50  0001 C CNN
	1    1700 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4_3
U 1 1 5C10507B
P 3650 5000
F 0 "R4_3" V 3750 5000 50  0000 C CNN
F 1 "1M" V 3800 5000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3580 5000 50  0001 C CNN
F 3 "~" H 3650 5000 50  0001 C CNN
	1    3650 5000
	0    1    1    0   
$EndComp
$Comp
L Device:R R3_3
U 1 1 5C105081
P 3650 4700
F 0 "R3_3" V 3650 4650 50  0000 C CNN
F 1 "1M" V 3534 4700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3580 4700 50  0001 C CNN
F 3 "~" H 3650 4700 50  0001 C CNN
	1    3650 4700
	0    1    1    0   
$EndComp
$Comp
L Device:C C2_3
U 1 1 5C105087
P 3650 4850
F 0 "C2_3" V 3700 4850 50  0000 C CNN
F 1 ".0068" V 3600 5050 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 3688 4700 50  0001 C CNN
F 3 "~" H 3650 4850 50  0001 C CNN
	1    3650 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 3950 1900 3950
Wire Wire Line
	1900 3950 1900 4000
Wire Wire Line
	1700 4250 1900 4250
Wire Wire Line
	1900 4250 1900 4200
$Comp
L Device:R_POT_US RV1_3
U 1 1 5C105091
P 3350 5000
F 0 "RV1_3" H 3282 5046 50  0000 R CNN
F 1 "1M" H 3282 4955 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 3350 5000 50  0001 C CNN
F 3 "~" H 3350 5000 50  0001 C CNN
	1    3350 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1_3
U 1 1 5C105097
P 2850 4100
F 0 "C1_3" V 2598 4100 50  0000 C CNN
F 1 "1uF" V 2689 4100 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 2888 3950 50  0001 C CNN
F 3 "~" H 2850 4100 50  0001 C CNN
	1    2850 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R2_3
U 1 1 5C10509D
P 3150 4100
F 0 "R2_3" V 2943 4100 50  0000 C CNN
F 1 "113K" V 3034 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3080 4100 50  0001 C CNN
F 3 "~" H 3150 4100 50  0001 C CNN
	1    3150 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R5_3
U 1 1 5C1050A3
P 4350 4200
F 0 "R5_3" V 4143 4200 50  0000 C CNN
F 1 "10K" V 4234 4200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4280 4200 50  0001 C CNN
F 3 "~" H 4350 4200 50  0001 C CNN
	1    4350 4200
	0    1    1    0   
$EndComp
$Comp
L Device:C C3_3
U 1 1 5C1050A9
P 4650 4400
F 0 "C3_3" V 4700 4600 50  0000 C CNN
F 1 ".0068" V 4600 4600 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 4688 4250 50  0001 C CNN
F 3 "~" H 4650 4400 50  0001 C CNN
	1    4650 4400
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6_3
U 1 1 5C1050AF
P 5000 4200
F 0 "J6_3" H 5079 4242 50  0000 L CNN
F 1 "V_out" H 5079 4151 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5000 4200 50  0001 C CNN
F 3 "~" H 5000 4200 50  0001 C CNN
	1    5000 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J1_3
U 1 1 5C1050B5
P 850 4000
F 0 "J1_3" H 750 4000 50  0000 L CNN
F 1 "A_electrode_+" H 1050 4050 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 850 4000 50  0001 C CNN
F 3 "~" H 850 4000 50  0001 C CNN
	1    850  4000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J2_3
U 1 1 5C1050BB
P 850 4100
F 0 "J2_3" H 750 4100 50  0000 L CNN
F 1 "A_electrode_-" H 1050 4150 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 850 4100 50  0001 C CNN
F 3 "~" H 850 4100 50  0001 C CNN
	1    850  4100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J3_3
U 1 1 5C1050C1
P 850 4200
F 0 "J3_3" H 750 4200 50  0000 L CNN
F 1 "N_electrode" H 1050 4250 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 850 4200 50  0001 C CNN
F 3 "~" H 850 4200 50  0001 C CNN
	1    850  4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 4000 1600 4000
Wire Wire Line
	1600 4000 1600 3900
Wire Wire Line
	1600 3900 1900 3900
Wire Wire Line
	1050 4100 1600 4100
Wire Wire Line
	1600 4100 1600 4300
Wire Wire Line
	1600 4300 1900 4300
NoConn ~ 2400 4400
Wire Wire Line
	3500 4850 3350 4850
Wire Wire Line
	3350 4850 3350 4700
Wire Wire Line
	3350 4100 3300 4100
Connection ~ 3350 4850
Wire Wire Line
	3350 4100 3550 4100
Connection ~ 3350 4100
Wire Wire Line
	3500 4700 3350 4700
Connection ~ 3350 4700
Wire Wire Line
	3350 4700 3350 4100
Wire Wire Line
	3800 4850 4200 4850
Wire Wire Line
	4200 4850 4200 4700
Wire Wire Line
	4200 4200 4150 4200
Wire Wire Line
	3800 4700 4200 4700
Connection ~ 4200 4700
Wire Wire Line
	4200 4700 4200 4200
Connection ~ 4200 4200
Wire Wire Line
	4500 4200 4650 4200
Wire Wire Line
	4650 4250 4650 4200
Connection ~ 4650 4200
Wire Wire Line
	4650 4200 4800 4200
NoConn ~ 3350 5150
NoConn ~ 3950 4500
NoConn ~ 3850 3900
Wire Wire Line
	1050 4200 1550 4200
Wire Wire Line
	1550 4500 3250 4500
Wire Wire Line
	3250 4500 3250 4300
Wire Wire Line
	3250 4300 3550 4300
Wire Wire Line
	1550 4200 1550 4500
Wire Wire Line
	4850 4800 4850 5200
Connection ~ 3250 4500
Wire Wire Line
	3250 5200 4850 5200
Wire Wire Line
	3250 5200 3250 4500
$Comp
L Amplifier_Instrumentation:AD620 U1_4
U 1 1 5C10A749
P 2300 5800
F 0 "U1_4" H 2250 5850 50  0000 L CNN
F 1 "AD620" H 2150 5750 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2300 5800 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/AD620.pdf" H 2300 5800 50  0001 C CNN
	1    2300 5800
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:CA3130 U2_4
U 1 1 5C10A74F
P 3850 5900
F 0 "U2_4" H 4050 5800 50  0000 L CNN
F 1 "3130" H 4050 6050 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3750 5800 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/ca31/ca3130-a.pdf" H 3850 5900 50  0001 C CNN
	1    3850 5900
	1    0    0    1   
$EndComp
$Comp
L Device:R R1_4
U 1 1 5C10A755
P 1700 5800
F 0 "R1_4" V 1700 5750 39  0000 L CNN
F 1 "2.2K" V 1600 5700 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1630 5800 50  0001 C CNN
F 3 "~" H 1700 5800 50  0001 C CNN
	1    1700 5800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4_4
U 1 1 5C10A75B
P 3650 6700
F 0 "R4_4" V 3750 6700 50  0000 C CNN
F 1 "1M" V 3800 6700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3580 6700 50  0001 C CNN
F 3 "~" H 3650 6700 50  0001 C CNN
	1    3650 6700
	0    1    1    0   
$EndComp
$Comp
L Device:R R3_4
U 1 1 5C10A761
P 3650 6400
F 0 "R3_4" V 3650 6350 50  0000 C CNN
F 1 "1M" V 3534 6400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3580 6400 50  0001 C CNN
F 3 "~" H 3650 6400 50  0001 C CNN
	1    3650 6400
	0    1    1    0   
$EndComp
$Comp
L Device:C C2_4
U 1 1 5C10A767
P 3650 6550
F 0 "C2_4" V 3700 6550 50  0000 C CNN
F 1 ".0068" V 3600 6750 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 3688 6400 50  0001 C CNN
F 3 "~" H 3650 6550 50  0001 C CNN
	1    3650 6550
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 5650 1900 5650
Wire Wire Line
	1900 5650 1900 5700
Wire Wire Line
	1700 5950 1900 5950
Wire Wire Line
	1900 5950 1900 5900
$Comp
L Device:R_POT_US RV1_4
U 1 1 5C10A771
P 3350 6700
F 0 "RV1_4" H 3282 6746 50  0000 R CNN
F 1 "1M" H 3282 6655 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 3350 6700 50  0001 C CNN
F 3 "~" H 3350 6700 50  0001 C CNN
	1    3350 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1_4
U 1 1 5C10A777
P 2850 5800
F 0 "C1_4" V 2598 5800 50  0000 C CNN
F 1 "1uF" V 2689 5800 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 2888 5650 50  0001 C CNN
F 3 "~" H 2850 5800 50  0001 C CNN
	1    2850 5800
	0    1    1    0   
$EndComp
$Comp
L Device:R R2_4
U 1 1 5C10A77D
P 3150 5800
F 0 "R2_4" V 2943 5800 50  0000 C CNN
F 1 "113K" V 3034 5800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3080 5800 50  0001 C CNN
F 3 "~" H 3150 5800 50  0001 C CNN
	1    3150 5800
	0    1    1    0   
$EndComp
$Comp
L Device:R R5_4
U 1 1 5C10A783
P 4350 5900
F 0 "R5_4" V 4143 5900 50  0000 C CNN
F 1 "10K" V 4234 5900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4280 5900 50  0001 C CNN
F 3 "~" H 4350 5900 50  0001 C CNN
	1    4350 5900
	0    1    1    0   
$EndComp
$Comp
L Device:C C3_4
U 1 1 5C10A789
P 4650 6100
F 0 "C3_4" V 4700 6300 50  0000 C CNN
F 1 ".0068" V 4600 6300 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 4688 5950 50  0001 C CNN
F 3 "~" H 4650 6100 50  0001 C CNN
	1    4650 6100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6_4
U 1 1 5C10A78F
P 5000 5900
F 0 "J6_4" H 5079 5942 50  0000 L CNN
F 1 "V_out" H 5079 5851 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 5000 5900 50  0001 C CNN
F 3 "~" H 5000 5900 50  0001 C CNN
	1    5000 5900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J1_4
U 1 1 5C10A795
P 850 5700
F 0 "J1_4" H 750 5700 50  0000 L CNN
F 1 "A_electrode_+" H 1050 5750 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 850 5700 50  0001 C CNN
F 3 "~" H 850 5700 50  0001 C CNN
	1    850  5700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J2_4
U 1 1 5C10A79B
P 850 5800
F 0 "J2_4" H 750 5800 50  0000 L CNN
F 1 "A_electrode_-" H 1050 5850 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 850 5800 50  0001 C CNN
F 3 "~" H 850 5800 50  0001 C CNN
	1    850  5800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J3_4
U 1 1 5C10A7A1
P 850 5900
F 0 "J3_4" H 750 5900 50  0000 L CNN
F 1 "N_electrode" H 1050 5950 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 850 5900 50  0001 C CNN
F 3 "~" H 850 5900 50  0001 C CNN
	1    850  5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5700 1600 5700
Wire Wire Line
	1600 5700 1600 5600
Wire Wire Line
	1600 5600 1900 5600
Wire Wire Line
	1050 5800 1600 5800
Wire Wire Line
	1600 5800 1600 6000
Wire Wire Line
	1600 6000 1900 6000
NoConn ~ 2400 6100
Wire Wire Line
	3500 6550 3350 6550
Wire Wire Line
	3350 6550 3350 6400
Wire Wire Line
	3350 5800 3300 5800
Connection ~ 3350 6550
Wire Wire Line
	3350 5800 3550 5800
Connection ~ 3350 5800
Wire Wire Line
	3500 6400 3350 6400
Connection ~ 3350 6400
Wire Wire Line
	3350 6400 3350 5800
Wire Wire Line
	3800 6550 4200 6550
Wire Wire Line
	4200 6550 4200 6400
Wire Wire Line
	4200 5900 4150 5900
Wire Wire Line
	3800 6400 4200 6400
Connection ~ 4200 6400
Wire Wire Line
	4200 6400 4200 5900
Connection ~ 4200 5900
Wire Wire Line
	4500 5900 4650 5900
Wire Wire Line
	4650 5950 4650 5900
Connection ~ 4650 5900
Wire Wire Line
	4650 5900 4800 5900
NoConn ~ 3350 6850
NoConn ~ 3850 6200
NoConn ~ 3950 6200
NoConn ~ 3850 5600
Wire Wire Line
	1050 5900 1550 5900
Wire Wire Line
	1550 6200 3250 6200
Wire Wire Line
	3250 6200 3250 6000
Wire Wire Line
	3250 6000 3550 6000
Wire Wire Line
	1550 5900 1550 6200
Wire Wire Line
	4850 6500 4850 6900
Connection ~ 3250 6200
Wire Wire Line
	3250 6900 4850 6900
Wire Wire Line
	3250 6900 3250 6200
$Comp
L Amplifier_Instrumentation:AD620 U1_5
U 1 1 5C112DCC
P 2250 8000
F 0 "U1_5" H 2200 8050 50  0000 L CNN
F 1 "AD620" H 2100 7950 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2250 8000 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/AD620.pdf" H 2250 8000 50  0001 C CNN
	1    2250 8000
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:CA3130 U2_5
U 1 1 5C112DD2
P 3800 8100
F 0 "U2_5" H 4000 8000 50  0000 L CNN
F 1 "3130" H 4000 8250 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3700 8000 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/ca31/ca3130-a.pdf" H 3800 8100 50  0001 C CNN
	1    3800 8100
	1    0    0    1   
$EndComp
$Comp
L Device:R R1_5
U 1 1 5C112DD8
P 1650 8000
F 0 "R1_5" V 1650 7950 39  0000 L CNN
F 1 "2.2K" V 1550 7900 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1580 8000 50  0001 C CNN
F 3 "~" H 1650 8000 50  0001 C CNN
	1    1650 8000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4_5
U 1 1 5C112DDE
P 3600 8900
F 0 "R4_5" V 3700 8900 50  0000 C CNN
F 1 "1M" V 3750 8900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3530 8900 50  0001 C CNN
F 3 "~" H 3600 8900 50  0001 C CNN
	1    3600 8900
	0    1    1    0   
$EndComp
$Comp
L Device:R R3_5
U 1 1 5C112DE4
P 3600 8600
F 0 "R3_5" V 3600 8550 50  0000 C CNN
F 1 "1M" V 3484 8600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3530 8600 50  0001 C CNN
F 3 "~" H 3600 8600 50  0001 C CNN
	1    3600 8600
	0    1    1    0   
$EndComp
$Comp
L Device:C C2_5
U 1 1 5C112DEA
P 3600 8750
F 0 "C2_5" V 3650 8750 50  0000 C CNN
F 1 ".0068" V 3550 8950 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 3638 8600 50  0001 C CNN
F 3 "~" H 3600 8750 50  0001 C CNN
	1    3600 8750
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 7850 1850 7850
Wire Wire Line
	1850 7850 1850 7900
Wire Wire Line
	1650 8150 1850 8150
Wire Wire Line
	1850 8150 1850 8100
$Comp
L Device:R_POT_US RV1_5
U 1 1 5C112DF4
P 3300 8900
F 0 "RV1_5" H 3232 8946 50  0000 R CNN
F 1 "1M" H 3232 8855 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 3300 8900 50  0001 C CNN
F 3 "~" H 3300 8900 50  0001 C CNN
	1    3300 8900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1_5
U 1 1 5C112DFA
P 2800 8000
F 0 "C1_5" V 2548 8000 50  0000 C CNN
F 1 "1uF" V 2639 8000 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 2838 7850 50  0001 C CNN
F 3 "~" H 2800 8000 50  0001 C CNN
	1    2800 8000
	0    1    1    0   
$EndComp
$Comp
L Device:R R2_5
U 1 1 5C112E00
P 3100 8000
F 0 "R2_5" V 2893 8000 50  0000 C CNN
F 1 "113K" V 2984 8000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3030 8000 50  0001 C CNN
F 3 "~" H 3100 8000 50  0001 C CNN
	1    3100 8000
	0    1    1    0   
$EndComp
$Comp
L Device:R R5_5
U 1 1 5C112E06
P 4300 8100
F 0 "R5_5" V 4093 8100 50  0000 C CNN
F 1 "10K" V 4184 8100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4230 8100 50  0001 C CNN
F 3 "~" H 4300 8100 50  0001 C CNN
	1    4300 8100
	0    1    1    0   
$EndComp
$Comp
L Device:C C3_5
U 1 1 5C112E0C
P 4600 8300
F 0 "C3_5" V 4650 8500 50  0000 C CNN
F 1 ".0068" V 4550 8500 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 4638 8150 50  0001 C CNN
F 3 "~" H 4600 8300 50  0001 C CNN
	1    4600 8300
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6_5
U 1 1 5C112E12
P 4950 8100
F 0 "J6_5" H 5029 8142 50  0000 L CNN
F 1 "V_out" H 5029 8051 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 4950 8100 50  0001 C CNN
F 3 "~" H 4950 8100 50  0001 C CNN
	1    4950 8100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J1_5
U 1 1 5C112E18
P 800 7900
F 0 "J1_5" H 700 7900 50  0000 L CNN
F 1 "A_electrode_+" H 1000 7950 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 800 7900 50  0001 C CNN
F 3 "~" H 800 7900 50  0001 C CNN
	1    800  7900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J2_5
U 1 1 5C112E1E
P 800 8000
F 0 "J2_5" H 700 8000 50  0000 L CNN
F 1 "A_electrode_-" H 1000 8050 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 800 8000 50  0001 C CNN
F 3 "~" H 800 8000 50  0001 C CNN
	1    800  8000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J3_5
U 1 1 5C112E24
P 800 8100
F 0 "J3_5" H 700 8100 50  0000 L CNN
F 1 "N_electrode" H 1000 8150 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 800 8100 50  0001 C CNN
F 3 "~" H 800 8100 50  0001 C CNN
	1    800  8100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 7900 1550 7900
Wire Wire Line
	1550 7900 1550 7800
Wire Wire Line
	1550 7800 1850 7800
Wire Wire Line
	1000 8000 1550 8000
Wire Wire Line
	1550 8000 1550 8200
Wire Wire Line
	1550 8200 1850 8200
NoConn ~ 2350 8300
Wire Wire Line
	3450 8750 3300 8750
Wire Wire Line
	3300 8750 3300 8600
Wire Wire Line
	3300 8000 3250 8000
Connection ~ 3300 8750
Wire Wire Line
	3300 8000 3500 8000
Connection ~ 3300 8000
Wire Wire Line
	3450 8600 3300 8600
Connection ~ 3300 8600
Wire Wire Line
	3300 8600 3300 8000
Wire Wire Line
	3750 8750 4150 8750
Wire Wire Line
	4150 8750 4150 8600
Wire Wire Line
	4150 8100 4100 8100
Wire Wire Line
	3750 8600 4150 8600
Connection ~ 4150 8600
Wire Wire Line
	4150 8600 4150 8100
Connection ~ 4150 8100
Wire Wire Line
	4450 8100 4600 8100
Wire Wire Line
	4600 8150 4600 8100
Connection ~ 4600 8100
Wire Wire Line
	4600 8100 4750 8100
NoConn ~ 3300 9050
NoConn ~ 3800 8400
NoConn ~ 3900 8400
NoConn ~ 3800 7800
Wire Wire Line
	1000 8100 1500 8100
Wire Wire Line
	1500 8400 3200 8400
Wire Wire Line
	3200 8400 3200 8200
Wire Wire Line
	3200 8200 3500 8200
Wire Wire Line
	1500 8100 1500 8400
Wire Wire Line
	4800 8700 4800 9100
Connection ~ 3200 8400
Wire Wire Line
	3200 9100 4800 9100
Wire Wire Line
	3200 9100 3200 8400
Wire Wire Line
	5850 1650 5850 3250
Wire Wire Line
	4800 8700 5850 8700
Wire Wire Line
	4850 6500 5850 6500
Connection ~ 5850 6500
Wire Wire Line
	5850 6500 5850 8700
Wire Wire Line
	4850 4800 5850 4800
Connection ~ 5850 4800
Wire Wire Line
	5850 4800 5850 6500
Wire Wire Line
	4900 3250 5850 3250
Connection ~ 5850 3250
Wire Wire Line
	5850 3250 5850 4800
Text Label 5650 4750 2    50   ~ 0
Virtual_GND
Text Label 5650 6450 2    50   ~ 0
Virtual_GND
Text Label 5650 8650 2    50   ~ 0
Virtual_GND
Wire Wire Line
	7300 1100 8200 1100
Wire Wire Line
	8200 1100 8200 2200
Wire Wire Line
	8200 2250 8650 2250
Connection ~ 7300 1100
Wire Wire Line
	7300 1100 7300 1200
Wire Wire Line
	7300 2000 7300 2250
Wire Wire Line
	7300 2750 8650 2750
Connection ~ 7300 2000
Wire Wire Line
	2250 2250 2250 2200
Wire Wire Line
	2250 2200 4400 2200
Connection ~ 8200 2200
Wire Wire Line
	8200 2200 8200 2250
Wire Wire Line
	2200 3800 2200 3700
Wire Wire Line
	2200 3700 4350 3700
Wire Wire Line
	8200 3700 8200 2250
Connection ~ 8200 2250
Wire Wire Line
	8200 3700 8200 5400
Wire Wire Line
	8200 7500 5350 7500
Wire Wire Line
	2150 7500 2150 7700
Connection ~ 8200 3700
Wire Wire Line
	2200 5500 2200 5400
Wire Wire Line
	2200 5400 4350 5400
Connection ~ 8200 5400
Wire Wire Line
	8200 5400 8200 7500
Wire Wire Line
	2250 2850 2250 3050
Wire Wire Line
	2250 3050 2900 3050
Wire Wire Line
	2900 3050 2900 2250
Wire Wire Line
	2900 2250 3800 2250
Connection ~ 7300 2250
Wire Wire Line
	7300 2250 7300 2750
Wire Wire Line
	2200 4450 2850 4450
Wire Wire Line
	2850 4450 2850 3750
Wire Wire Line
	2850 3750 3750 3750
Wire Wire Line
	7300 3750 7300 2750
Connection ~ 7300 2750
Wire Wire Line
	7300 3750 7300 5450
Wire Wire Line
	7300 5450 5450 5450
Wire Wire Line
	2850 5450 2850 6150
Wire Wire Line
	2850 6150 2200 6150
Wire Wire Line
	2200 6150 2200 6100
Connection ~ 7300 3750
Wire Wire Line
	2150 8300 2150 8350
Wire Wire Line
	2150 8350 2800 8350
Wire Wire Line
	2800 8350 2800 7550
Wire Wire Line
	2800 7550 3700 7550
Wire Wire Line
	7300 7550 7300 5450
Connection ~ 7300 5450
Wire Wire Line
	3700 7800 3700 7550
Connection ~ 3700 7550
Wire Wire Line
	3700 7550 5250 7550
Wire Wire Line
	4600 8450 5250 8450
Wire Wire Line
	5250 8450 5250 7550
Connection ~ 5250 7550
Wire Wire Line
	5250 7550 7300 7550
Wire Wire Line
	3750 8900 4700 8900
Wire Wire Line
	4700 8900 4700 8550
Wire Wire Line
	4700 8550 5350 8550
Wire Wire Line
	5350 8550 5350 7500
Connection ~ 5350 7500
Wire Wire Line
	5350 7500 4300 7500
Wire Wire Line
	3700 8400 3700 8500
Wire Wire Line
	3700 8500 4300 8500
Wire Wire Line
	4300 8500 4300 7500
Connection ~ 4300 7500
Wire Wire Line
	4300 7500 2150 7500
Wire Wire Line
	3800 6700 4800 6700
Wire Wire Line
	4800 6700 4800 6050
Wire Wire Line
	4800 6050 5300 6050
Wire Wire Line
	5300 6050 5300 5400
Connection ~ 5300 5400
Wire Wire Line
	5300 5400 8200 5400
Wire Wire Line
	4650 6250 5450 6250
Wire Wire Line
	5450 6250 5450 5450
Connection ~ 5450 5450
Wire Wire Line
	5450 5450 3750 5450
Wire Wire Line
	3750 6200 3750 6250
Wire Wire Line
	3750 6250 4350 6250
Wire Wire Line
	4350 6250 4350 5400
Connection ~ 4350 5400
Wire Wire Line
	4350 5400 5300 5400
Wire Wire Line
	3750 5600 3750 5450
Connection ~ 3750 5450
Wire Wire Line
	3750 5450 2850 5450
Wire Wire Line
	3800 5000 4800 5000
Wire Wire Line
	4800 5000 4800 4500
Wire Wire Line
	4800 4500 5350 4500
Wire Wire Line
	5350 4500 5350 3700
Connection ~ 5350 3700
Wire Wire Line
	5350 3700 8200 3700
Wire Wire Line
	4650 4550 5300 4550
Wire Wire Line
	5300 4550 5300 3750
Connection ~ 5300 3750
Wire Wire Line
	5300 3750 7300 3750
Wire Wire Line
	3750 3900 3750 3750
Connection ~ 3750 3750
Wire Wire Line
	3750 3750 5300 3750
Connection ~ 4350 3700
Wire Wire Line
	4350 3700 5350 3700
Wire Wire Line
	3800 2350 3800 2250
Connection ~ 3800 2250
Wire Wire Line
	3800 2250 5400 2250
Wire Wire Line
	3850 3450 4850 3450
Wire Wire Line
	4850 3450 4850 3050
Wire Wire Line
	4850 3050 5500 3050
Wire Wire Line
	5500 3050 5500 2200
Connection ~ 5500 2200
Wire Wire Line
	5500 2200 8200 2200
Wire Wire Line
	4700 3000 5400 3000
Wire Wire Line
	5400 3000 5400 2250
Connection ~ 5400 2250
Wire Wire Line
	5400 2250 7300 2250
Wire Wire Line
	3800 2950 3800 3050
Wire Wire Line
	3800 3050 4400 3050
Wire Wire Line
	4400 3050 4400 2200
Connection ~ 4400 2200
Wire Wire Line
	4400 2200 5500 2200
Wire Wire Line
	3750 4500 3750 4600
Wire Wire Line
	3750 4600 4350 4600
Wire Wire Line
	4350 3700 4350 4600
Wire Wire Line
	2200 4400 2200 4450
Text Label 3800 2200 0    50   ~ 0
VCC
Text Label 4600 2350 0    50   ~ 0
GNd
NoConn ~ 3850 4500
$EndSCHEMATC
