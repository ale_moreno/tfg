// THE ARDUINO CODE FOR EEG



#define ANALOG_IN 0



void setup() {

  Serial.begin(19200);
  analogReference(INTERNAL);

}



void loop() {

  int val = analogRead(ANALOG_IN);

  Serial.write( 0xff );
  Serial.write( (byte)(val >> 8) );
  Serial.write( val & 0xff );

}
