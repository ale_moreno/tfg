#include "arduinoFFT.h"

#define SAMPLES 512             //Must be a power of 2
#define SAMPLING_FREQUENCY 200  //Hz, must be less than 10000 due to ADC

arduinoFFT FFT = arduinoFFT();

double vReal[SAMPLES];
double vImag[SAMPLES];

float ANALOG_IN = A1;
int val = 0;
float measureT = 0.0;
float Delta = 0.0;
float Theta = 0.0;
float Alpha = 0.0;
float Beta = 0.0;
float Gamma = 0.0;
float freq = 0.0;
float total = 0.0;

float MeasureT(int msInterval, float(*funct)())
{
   float sum=0.0;
   long startTime = millis();
   int samplesNumber = 0;
 
   while (millis() - startTime < msInterval)
   {
      samplesNumber++;
      sum += funct();
   }

   return sum / samplesNumber;
}
 
float getMeasure()
{
   return val = analogRead(ANALOG_IN);
}

float frequencies(){
  for(int i=0; i<SAMPLES; i++)
    {
        measureT = MeasureT(80, getMeasure);
        vReal[i] = measureT;
        vImag[i] = 0;
//        Serial.print("Muestras: "+ String(i) + "\t");
        Serial.println(String(measureT) + "," + String(Delta,10) + "," + String(Theta,10) + "," + String(Alpha,10) + "," + String(Beta,10) + "," + String(Gamma,10));    
    }

    FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
    FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD);
    FFT.ComplexToMagnitude(vReal, vImag, SAMPLES);

    for(int i=0; i<(SAMPLES/2); i++)
    {
        /*View all these three lines in serial terminal to see which frequencies has which amplitudes*/
        total += vReal[i];
//        Serial.print((i * 1.0 * SAMPLING_FREQUENCY) / SAMPLES, 1);
//        Serial.print(" ");
//        Serial.println("****************VREAL**************");
//        Serial.println(vReal[i], 1);    //View only this line in serial plotter to visualize the bins
//        Serial.println("****************VREAL**************");

        freq = (i * 1.0 * SAMPLING_FREQUENCY) / SAMPLES, 1;
//        Serial.println("********** Freq = " + String(freq) + "*************");
        if(freq >= 0.5 && freq <= 3.5)
            Delta += vReal[i];
        else if(freq > 3.5 && freq <= 7.5)
            Theta += vReal[i];
        else if(freq > 7.5 && freq <= 13)
            Alpha += vReal[i];
        else if(freq > 13 && freq <= 30) 
            Beta += vReal[i];
        else if(freq > 30 && freq <= 60)
            Gamma += vReal[i];
    }

      Delta /= total;
      Theta /= total;
      Alpha /= total;
      Beta /= total;
      Gamma /= total;

      Serial.println(String(measureT) + "," + String(Delta,10) + "," + String(Theta,10) + "," + String(Alpha,10) + "," + String(Beta,10) + "," + String(Gamma,10));
    
  }

void setup()
{
    Serial.begin(57600);
}
 
void loop()
{
  // Bloque de estudio para Matlab
  // measureT = MeasureT(80, getMeasure);//Lobulo occipital
  // measureT = MeasureT(100, getMeasure);//Lobulo frontal Fp1-Fp2
  
  frequencies();

//  Serial.println("Signal %d, Delta %e, Theta %e, Alpha %e, Beta %e, Gamma %e", measureT, Delta, Theta, Alpha, Beta, Gamma);
}
