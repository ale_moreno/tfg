#define ANALOG_IN 1
int val = 0;
float measureT = 0.0;
float measureN = 0.0;


float min, max, mid;
float MeasureN(int samplesNumber, float(*funct)())
{
   float value;
 
   for (int i = 0; i < samplesNumber; i++)
   {
      value = funct();
      if (value > max) max = value;
      if (value < min) min = value;
   }
 
   mid = (max - min) / 2;
   return mid / samplesNumber;
}
 
float MeasureT(int msInterval, float(*funct)())
{
   float value;
   long startTime = millis();
 
   while (millis() - startTime < msInterval)
   {
      value = funct();
      if (value > max) max = value;
      if (value < min) min = value;
   }
   
   mid = (max - min) / 2;
   return mid / msInterval;
}

float getMeasure()
{
  return val = analogRead(ANALOG_IN);
}
 
void setup()
{
   Serial.begin(57600);
   
}
 
void loop()
{
      measureN=MeasureN(100, getMeasure);
      measureT=MeasureT(50, getMeasure);
      Serial.println(measureT);
      Serial.println(measureN);
      delay(10);
  
 
} 
