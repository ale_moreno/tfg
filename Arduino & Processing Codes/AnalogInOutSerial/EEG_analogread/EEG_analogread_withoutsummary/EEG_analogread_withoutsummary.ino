
#define CHANNEL0 0
#define CHANNEL1 1
#define CHANNEL2 2
#define CHANNEL3 3
#define CHANNEL4 4

float val_channel0 = 0;
float val_channel1 = 0;
float val_channel2 = 0;
float val_channel3 = 0;
float val_channel4 = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
}

void loop() {
  // put your main code here, to run repeatedly:
    val_channel0 = analogRead(CHANNEL0);
    val_channel1 = analogRead(CHANNEL1);
    val_channel2 = analogRead(CHANNEL2);
    val_channel3 = analogRead(CHANNEL3);
    val_channel4 = analogRead(CHANNEL4);
    
    Serial.print(val_channel0);
    Serial.print(",");
    Serial.print(val_channel1);
    Serial.print(",");
    Serial.print(val_channel2);
    Serial.print(",");
    Serial.print(val_channel3);
    Serial.print(",") ;
    Serial.println(val_channel4);
}
