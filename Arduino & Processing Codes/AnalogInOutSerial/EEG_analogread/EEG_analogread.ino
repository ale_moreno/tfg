float ANALOG_IN = A1;
int val = 0;
float measureT = 0.0;


float MeasureT(int msInterval, float(*funct)())
{
   float sum=0.0;
   long startTime = millis();
   int samplesNumber = 0;
 
   while (millis() - startTime < msInterval)
   {
      samplesNumber++;
      sum += funct();
   }

   return sum / samplesNumber;
}
 
float getMeasure()
{
   return val = analogRead(ANALOG_IN);
}


void setup()
{
    Serial.begin(57600);
}
 
void loop()
{
  // Bloque de estudio para Matlab
  measureT = MeasureT(80, getMeasure);//Lobulo occipital
  // measureT = MeasureT(100, getMeasure);//Lobulo frontal Fp1-Fp2

  Serial.println(measureT);
}
