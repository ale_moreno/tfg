import processing.serial.*;

Serial myport;
String dataReading = "";
String[] dataOutput = {
};
String log;

void setup () {
  size(100, 100);
 
  myport = new Serial(this, Serial.list()[0], 57600);
  myport.bufferUntil('\n');
}

void draw(){
  
     while (myport.available() > 0) {
      println(myport.readStringUntil('\n'));
  }
  
}

void serialEvent(Serial myPort) {
  dataReading = myPort.readStringUntil('\n');
  println(dataReading);
  if (dataReading != null) {
    dataOutput = append(dataOutput, dataReading);
    if (dataReading.substring(0, 3).equals("LOG")) {
      log = dataReading.substring(3);
      println(log);
      if(dataReading.substring(3).equals("visual stim")) {
      //  String logVisStim = dataReading.substring(3); //here I tried to create a separate string for the substrings after LOG.
      //  println(logVisStim);//This is to test the line of code above.
        background(255);
      }else if(dataReading.substring(3).equals("vis left error")){
        background(0);
      }else{
        background(100);
      }
    }
  }
}
 
  void saveData() {
    println("saving to txt file...");
    saveStrings("./data.txt", dataOutput);
  }
 
 
  void mousePressed() {
    saveData();
  }
