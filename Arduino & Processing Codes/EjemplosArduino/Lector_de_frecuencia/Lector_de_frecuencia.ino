#include <FreqCount.h>

int Analog = A0;

void setup() {
  Serial.begin(57600);
  FreqCount.begin(1000);
}

void loop() {
  if (FreqCount.available()) {
    unsigned long count = FreqCount.read();
    Serial.print(count);
    Serial.print(",");
    Serial.println(analogRead(Analog));
  }
}
