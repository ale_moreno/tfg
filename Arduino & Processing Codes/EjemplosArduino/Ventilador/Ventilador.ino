int motor = A0;
int regulador = A1;


void setup() {
  // put your setup code here, to run once:
  pinMode(motor,OUTPUT);
  pinMode(regulador,INPUT);

  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  int _regulador=analogRead(regulador);
  int _reg = map(_regulador, 15, 700, 0, 125);
  Serial.println("Valor del regulador:");
  Serial.println(_regulador);

  Serial.println("*********Valor del regulador mapeado:");
  Serial.println(_reg);
  
  analogWrite(motor, _reg);
  
  delay(100);
}
