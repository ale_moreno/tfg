from tkinter import *
import tkinter as tk # proper way to import tkinter
import serial

import threading

class Dee(tk.Frame):
    def __init__(self, master=None, title='', ylabel='', label='', color='c', ylim=1, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.data = []
        fig = Figure(figsize = (7,6))
        self.plot = fig.add_subplot(111)
        self.plot.set_title(title)
        self.plot.set_ylabel(ylabel)
        self.plot.set_ylim(0, ylim)
        self.line, = self.plot.plot([], [], color, marker = 'o',label = label)
        self.plot.legend(loc='upper left')

        label = Label(self, text = ylabel, relief = "solid", font = "Verdana 22 bold")
        label.grid(row = 0, column = 3)
        button_1 = Button(self, text = "Back To Homepage", command = F1.tkraise)
        button_1.grid(row = 1, column = 2)
        label_1 = Label(self, text = "Current Value: ", relief = "solid", font = "Verdana 10 bold")
        label_1.grid(row = 2, column = 2)
        self.label_data = Label(self, font = "Verdana 10")
        self.label_data.grid(row = 2, column = 3)
        canvas = FigureCanvasTkAgg(fig, master=self)
        canvas.get_tk_widget().grid(row = 3, column = 3)

        ani = animation.FuncAnimation(fig, self.update_graph, interval = 1000, blit = False)
        canvas.draw()

    def update_graph(self, i):
        if self.data:
            self.line.set_data(range(len(self.data)), self.data)
            self.plot.set_xlim(0, len(self.data))

    def set(self, value):
        self.data.append(value)
        self.label_data.config(text=value)

my_window = Tk()
my_window.title("Graphical User Interface EEG")
my_window.geometry("900x900")

F1 = Frame(my_window, relief = RAISED)
F2 = Dee(my_window, title='Channel 0', ylabel='Chan 0', color='b', label='Units', ylim=100, relief = RAISED)
F3 = Dee(my_window, title='Channel 1', ylabel='Chan 1', color='b', label='Units', ylim=100, relief = RAISED)
F4 = Dee(my_window, title='Channel 2', ylabel='Chan 2', color='b', label='Units', ylim=100, relief = RAISED)
F5 = Dee(my_window, title='Channel 3', ylabel='Chan 3', color='b', label='Units', ylim=100, relief = RAISED)
F6 = Dee(my_window, title='Channel 4', ylabel='Chan 5', color='b', label='Units', ylim=100, relief = RAISED)

#For Frame One
label_1 = Label(F1, text = "Home", relief = "solid", font = "Verdana 22 bold")
label_1.grid(row = 0, column = 3)
button_1 = Button(F1, text = "Channel 0", relief = GROOVE, bd = 2, command = F2.tkraise)
button_1.grid(row = 1, column = 2)
button_2 = Button(F1, text = "Channel 1", relief = GROOVE, bd = 2, command = F3.tkraise)
button_2.grid(row = 1, column = 3)
button_3 = Button(F1, text = "Channel 2", relief = GROOVE, bd = 2, command = F4.tkraise)
button_3.grid(row = 1, column = 4)
button_4 = Button(F1, text = "Channel 3", relief = GROOVE, bd = 2, command = F5.tkraise)
button_4.grid(row = 1, column = 5)
button_5 = Button(F1, text = "Channel 4", relief = GROOVE, bd = 2, command = F6.tkraise)
button_5.grid(row = 1, column = 6)

for frame in(F1, F2, F3, F4, F5, F6):
    frame.grid(row = 0, column = 0, sticky = "NSEW")

F1.tkraise()

def get_data():
    #Initialization of Serial Comm
    ser = serial.Serial('COM3', 9600)
    while True:
        pulldata = ser.readline().decode('utf-8')
        get_data = pulldata.split(',')
        F2.set(int(get_data[0]))
        F3.set(int(get_data[1]))
        F4.set(int(get_data[3]))

# Start the thread that will poll the arduino
t = threading.Thread(target=get_data)
t.daemon = True
t.start()

my_window.mainloop()