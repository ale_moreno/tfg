from random import *
from tkinter import *

import matplotlib

matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.animation as animation
import serial
import threading


class CCanvas:

    # Program's variables

    Run = 0

    Ch0_on = 0
    Ch1_on = 0
    Ch2_on = 0
    Ch3_on = 0
    Ch4_on = 0

    Channel0_data = []
    Channel1_data = []
    Channel2_data = []
    Channel3_data = []
    Channel4_data = []

    FFT_ch0 = []
    FFT_ch1 = []
    FFT_ch2 = []
    FFT_ch3 = []
    FFT_ch4 = []

    Delta = 0
    Theta = 0
    Alpha = 0
    Beta = 0
    Gamma = 0

    def __init__(self, canvas, ):

        self.data = []
        self.canvas = canvas
        canvas.geometry("1024x1024")
        canvas.configure(bg = '#666666')
        canvas.title("Funcionamiento EEG, TFG Universidad de Huelva 2019")

        # Frame declaration
        self.ButtonFrame = Frame(canvas, width = "1024", heigh = "30")

        # Frame printing
        self.ButtonFrame.pack(side = TOP)

        # Buttons declaration
        self.Button_method("Play", self.play)
        self.Button_method("Stop", self.stop)
        self.Button_method("Cha0", lambda: self.Channel(0))
        self.Button_method("Cha1", lambda: self.Channel(1))
        self.Button_method("Cha2", lambda: self.Channel(2))
        self.Button_method("Cha3", lambda: self.Channel(3))
        self.Button_method("Cha4", lambda: self.Channel(4))

        # Diagram chart declaration

        self.Diagram_figure = Figure(figsize = (5,4))
        self.Diagram_chart = self.Diagram_figure.add_subplot(111)
        self.Diagram_chart.set_title("EEG diagram")
        self.Diagram_chart.set_ylabel('Signal potency')
        self.Diagram_chart.set_xlabel('Time ms')
        self.Diagram_chart.set_ylim(0, 500)
        self.line_ch0, = self.Diagram_chart.plot([], [], 'red', marker = ' ')
        self.line_ch1, = self.Diagram_chart.plot([], [], 'blue', marker = ' ')
        self.line_ch2, = self.Diagram_chart.plot([], [], 'green', marker = ' ')
        self.line_ch3, = self.Diagram_chart.plot([], [], 'orange', marker = ' ')
        self.line_ch4, = self.Diagram_chart.plot([], [], 'yellow', marker = ' ')
        self.Diagram_chart.legend(loc='upper left')

        self.D_canvas = FigureCanvasTkAgg(self.Diagram_figure, self.canvas)
        self.D_canvas.get_tk_widget().pack(side=TOP, fill = BOTH, expand = 1)

        ani_d = animation.FuncAnimation(self.Diagram_figure, self.update_plot, interval = 200)
        self.D_canvas.draw()

        # Frequencies Bar chart declaration

        self.f = Figure(figsize = (5,4), dpi=100)
        self.bar_chart = self.f.add_subplot(111)

        self.Frequencies = ('Delta', 'Theta', 'Alpha', 'Beta', 'Gamma')
        self.data_chart = (self.ale(), self.ale(), self.ale(), self.ale(), self.ale())
        self.width = .5

        self.bar_chart.bar(self.Frequencies, self.data_chart, self.width)

        self.c = FigureCanvasTkAgg(self.f, self.canvas)
        self.c.get_tk_widget().pack(side=BOTTOM, fill = BOTH, expand = 1)

        ani = animation.FuncAnimation(self.f, self.update_graph, interval = 1000)

        self.c.draw()

    # Class functions

    def update_graph(self, i):
        self.bar_chart.clear()
        self.data_chart = (self.ale(), self.ale(), self.ale(), self.ale(), self.ale())
        self.bar_chart.bar(self.Frequencies, self.data_chart, self.width)

    def update_plot(self, i):
        self.line_ch0.set_data(100, self.ale())
        self.line_ch1.set_data(100, self.ale())
        self.line_ch2.set_data(100, self.ale())
        self.line_ch3.set_data(100, self.ale())
        self.line_ch4.set_data(100, self.ale())

    def Button_method(self, _text, _command):
        self.Button = Button(self.ButtonFrame, text = _text, command = _command, width = "19")
        self.Button.pack(side = LEFT)

    # Functions to take different signals for channels, play and pause

    def play(self):
        self.Run = 1
        print("Presionando play")

    def stop(self):
        self.Run = 0
        self.Channel0_data.clear()
        self.Channel1_data.clear()
        self.Channel2_data.clear()
        self.Channel3_data.clear()
        self.Channel4_data.clear()
        print("Presionando stop")

    def Channel(self, _channel):
        if _channel == 0:
            if self.Ch0_on:
                self.Ch0_on = 0
            else:
                self.Ch0_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch0_on)

        if _channel == 1:
            if self.Ch1_on:
                self.Ch1_on = 0
            else:
                self.Ch1_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch1_on)

        if _channel == 2:
            if self.Ch2_on:
                self.Ch2_on = 0
            else:
                self.Ch2_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch2_on)

        if _channel == 3:
            if self.Ch3_on:
                self.Ch3_on = 0
            else:
                self.Ch3_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch3_on)

        if _channel == 4:
            if self.Ch4_on:
                self.Ch4_on = 0
            else:
                self.Ch4_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch4_on)


    def ale(self):
        return randint(10, 500)

    def set(self, channel, data):
        if self.Run == 1:
            if channel == 0:
                if self.Ch0_on == 1:
                    Window_canvas.Channel0_data.append(data)
                else:
                    Window_canvas.Channel0_data.append(0)
            elif channel == 1:
                if self.Ch1_on == 1:
                    Window_canvas.Channel1_data.append(data)
                else:
                    Window_canvas.Channel1_data.append(0)
            elif channel == 2:
                if self.Ch2_on == 1:
                    Window_canvas.Channel2_data.append(data)
                else:
                    Window_canvas.Channel2_data.append(0)
            elif channel == 3:
                if self.Ch3_on == 1:
                    Window_canvas.Channel3_data.append(data)
                else:
                    Window_canvas.Channel3_data.append(0)
            else:
                if self.Ch4_on == 1:
                    Window_canvas.Channel4_data.append(data)
                else:
                    Window_canvas.Channel4_data.append(0)
        else:
            Window_canvas.Channel0_data.append(0)
            Window_canvas.Channel1_data.append(0)
            Window_canvas.Channel2_data.append(0)
            Window_canvas.Channel3_data.append(0)
            Window_canvas.Channel4_data.append(0)



# Class function callings

Window = Tk()
Window_canvas = CCanvas(Window)


def get_data():
    #Initialization of Serial Comm
    ser = serial.Serial('COM3', 9600)
    while True:
        pulldata = ser.readline().decode('utf-8')
        get_data = pulldata.split(',')
        Window_canvas.set(0, int(get_data[0]))
        Window_canvas.set(1, int(get_data[1]))
        Window_canvas.set(2, int(get_data[2]))
        Window_canvas.set(3, int(get_data[3]))
        Window_canvas.set(4, int(get_data[4]))



t_getdata = threading.Thread(target = get_data)
# t_FFT = threading.Thread(target = FFT)
t_getdata.daemon = True
# t_FFT.daemon = True
t_getdata.start()
# t_FFT.start()

Window.mainloop()