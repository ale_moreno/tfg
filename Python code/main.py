from tkinter import *
from random import *

import matplotlib

matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.animation as animation
import threading

from Frequencies import Frequencies
from Get_data import get_data
from queue import Queue

# Global variables
Run = 1

numchannels = 5
numFreques = 5

# Datas of communication between threads and Canvas
Channel0 = []
Channel1 = []
Channel2 = []
Channel3 = []
Channel4 = []



# Canvas

class CCanvas:
    Ch0_on = 0
    Ch1_on = 0
    Ch2_on = 0
    Ch3_on = 0
    Ch4_on = 0

    def __init__(self, canvas, __Frequencies, Run):
        self.data = []
        self.canvas = canvas
        self.Run = Run

        self.Frequencies = __Frequencies

        canvas.geometry("1024x1024")
        canvas.configure(bg = '#666666')
        canvas.title("Funcionamiento EEG, TFG Universidad de Huelva 2019")

        self.backgroundImage = PhotoImage("C:\Desktop\TFG\Python code\image")
        self.backgroundLabel = Label(canvas,image=self.backgroundImage)
        self.backgroundLabel.pack()

        # Frame declaration
        self.ButtonFrame = Frame(canvas, width = "1024", heigh = "30")

        # Frame printing
        # self.ButtonFrame.pack(side = TOP)

        # Buttons declaration
        self.Button_method("Play", self.play)
        self.Button_method("Stop", self.stop)
        self.Button_method("Cha0", lambda: self.Channel(0))
        self.Button_method("Cha1", lambda: self.Channel(1))
        self.Button_method("Cha2", lambda: self.Channel(2))
        self.Button_method("Cha3", lambda: self.Channel(3))
        self.Button_method("Cha4", lambda: self.Channel(4))

        # Diagram chart declaration

        self.Diagram_figure = Figure(figsize = (5, 4))
        self.Diagram_chart = self.Diagram_figure.add_subplot(111)
        self.Diagram_chart.set_title("EEG diagram")
        self.Diagram_chart.set_ylabel('Potencia se?al')
        self.Diagram_chart.set_xlabel('Muestras')
        self.Diagram_chart.set_ylim(150, 600)
        self.line_ch0, = self.Diagram_chart.plot([], [], 'red', marker = ' ', label = 'Channel 0')
        self.line_ch1, = self.Diagram_chart.plot([], [], 'blue', marker = ' ', label = 'Channel 1')
        self.line_ch2, = self.Diagram_chart.plot([], [], 'green', marker = ' ', label = 'Channel 2')
        self.line_ch3, = self.Diagram_chart.plot([], [], 'orange', marker = ' ', label = 'Channel 3')
        self.line_ch4, = self.Diagram_chart.plot([], [], 'yellow', marker = ' ', label = 'Channel 4')
        self.Diagram_chart.legend(loc = 'upper left')

        self.D_canvas = FigureCanvasTkAgg(self.Diagram_figure, self.canvas)
        self.D_canvas.get_tk_widget().pack(side = TOP, fill = BOTH, expand = 1)

        ani_d = animation.FuncAnimation(self.Diagram_figure, self.update_plot, interval = 10)
        self.D_canvas.draw()

        # Frequencies Bar chart declaration

        self.f = Figure(figsize = (5, 4), dpi = 100)
        self.bar_chart = self.f.add_subplot(111)
        self.bar_chart.set_ylim(0, 0.0050)

        self.freque = ('Delta', 'Theta', 'Alpha', 'Beta', 'Gamma')
        self.data_chart = (self.Frequencies.get_delta(), self.Frequencies.get_theta(), self.Frequencies.get_alpha(), self.Frequencies.get_beta(), self.Frequencies.get_gamma())
        self.width = .5

        self.bar_chart.bar(self.freque, self.data_chart, self.width)

        self.c = FigureCanvasTkAgg(self.f, self.canvas)
        self.c.get_tk_widget().pack(side = BOTTOM, fill = BOTH, expand = 1)

        ani = animation.FuncAnimation(self.f, self.update_graph, interval = 1000)

        self.c.draw()

    # Class functions

    def update_graph(self, i):
        self.bar_chart.clear()
        self.data_chart = (self.Frequencies.get_delta(), self.Frequencies.get_theta(), self.Frequencies.get_alpha(), self.Frequencies.get_beta(), self.Frequencies.get_gamma())
        self.bar_chart.bar(self.freque, self.data_chart, self.width)

    def update_plot(self, i):
        self.line_ch0.set_data(range(len(Channel0)), Channel0)
        self.line_ch1.set_data(range(len(Channel0)), Channel1)
        self.line_ch2.set_data(range(len(Channel0)), Channel2)
        self.line_ch3.set_data(range(len(Channel0)), Channel3)
        self.line_ch4.set_data(range(len(Channel0)), Channel4)
        self.Diagram_chart.set_xlim(len(Channel0)-200, len(Channel0))

    def Button_method(self, _text, _command):
        self.Button = Button(self.ButtonFrame, text = _text, command = _command, width = "19")
        self.Button.pack(side = LEFT)

    # Functions to take different signals for channels, play and pause

    def play(self):
        self.Run = 1

    def stop(self):
        self.Run = 0

    def ale(self):
        return randint(10, 500)

    def Channel(self, _channel):
        if _channel == 0:
            if self.Ch0_on:
                self.Ch0_on = 0
            else:
                self.Ch0_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch0_on)

        if _channel == 1:
            if self.Ch1_on:
                self.Ch1_on = 0
            else:
                self.Ch1_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch1_on)

        if _channel == 2:
            if self.Ch2_on:
                self.Ch2_on = 0
            else:
                self.Ch2_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch2_on)

        if _channel == 3:
            if self.Ch3_on:
                self.Ch3_on = 0
            else:
                self.Ch3_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch3_on)

        if _channel == 4:
            if self.Ch4_on:
                self.Ch4_on = 0
            else:
                self.Ch4_on = 1
            print("Presionando canal", _channel, " Channel: ", self.Ch4_on)


F_class = Frequencies()

Window = Tk()
Windows_canvas = CCanvas(Window, F_class, Run)


t_getdata = threading.Thread(target = get_data, args = (Channel0,Channel1,Channel2,Channel3, Channel4, Run, F_class))
t_getdata.daemon = True
t_getdata.start()

Window.mainloop()
