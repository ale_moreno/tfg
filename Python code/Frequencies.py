from numpy import fft


class Frequencies:
    def __init__(self):

        self.N = 200
        self.T = 1.0 / 800.0

        self.Delta = 0.0
        self.Theta = 0.0
        self.Alpha = 0.0
        self.Beta = 0.0
        self.Gamma = 0.0

        self._FFT = []

    def get_alpha(self):
        return self.Alpha

    def get_beta(self):
        return self.Beta

    def get_theta(self):
        return self.Theta

    def get_delta(self):
        return self.Delta

    def get_gamma(self):
        return self.Gamma

    def set_delta(self, data):
        self.Delta = data

    def set_theta(self,data):
        self.Theta = data

    def set_alpha(self,data):
        self.Alpha = data

    def set_beta(self,data):
        self.Beta = data

    def set_gamma(self,data):
        self.Gamma = data

    # def get_FFT(self, channel):
    #     # Devuelve las frecuencias de la señal en channel
    #     try:
    #         return fft.fft(channel, 3000, -1, 200)
    #     except NameError:
    #         print("Error al ejecutar FFT" + NameError)
    #
    # def median(self, Channels, Freqs):
    #
    #     print("Has llamado a una función de Frequencies")
    #     if len(Channels[0]) <= 100:
    #         self._FFT = self.get_FFT(Channels[0]) + self.get_FFT(Channels[1]) + self.get_FFT(
    #             Channels[2]) + self.get_FFT(Channels[3]) + self.get_FFT(Channels[4])
    #         self._FFT /= 4
    #
    #     total = 0.0
    #     cont = 0
    #     cont25 = 0
    #
    #     for i in self._FFT:
    #         total += self._FFT[i]
    #
    #     while 0.5 <= self._FFT[cont] <= 3.5:
    #         self.Delta += self._FFT[cont]
    #         cont += 1
    #
    #     while 3.5 < self._FFT[cont] <= 7.5:
    #         self.Theta += self._FFT[cont]
    #         cont += 1
    #
    #     while 7.5 < self._FFT[cont] <= 13:
    #         self.Alpha += self._FFT[cont]
    #         cont += 1
    #
    #     while 13 < self._FFT[cont] <= 30:
    #         self.Beta += self._FFT[cont]
    #         cont += 1
    #         if self._FFT[cont] <= 25:
    #             cont25 = cont
    #
    #     cont = cont25
    #     while 25 < self._FFT[cont] <= 40:
    #         self.Theta += self._FFT[cont]
    #         cont += 1
    #
    #     self.Alpha /= total
    #     self.Beta /= total
    #     self.Theta /= total
    #     self.Gamma /= total
    #     self.Delta /= total
    #
    #     Freqs[0].append(self.get_delta())
    #     Freqs[1].append(self.get_theta())
    #     Freqs[2].append(self.get_alpha())
    #     Freqs[3].append(self.get_beta())
    #     Freqs[4].append(self.get_gamma())
