# TFG #

Implementación de programas en arduino de ejemplos para el aprendizaje del sistema.


+ VERY IMPORTANT
* https://www.mouser.es/ds/2/609/ADUX1020-961389.pdf

# Bibliografia #

Se utilizará la bibliografía de la página SparkFun para la recolección de componentes.

* https://www.mouser.es/new/sparkfun/sparkfun-myoware-development-kit/
* https://www.mouser.es/Sensors/Sensor-Development-Tools/_/N-d0qxl/
* https://www.mouser.es/Sensors/Biomedical-Sensors/_/N-cbp2o/
* https://www.mouser.es/Sensors/Motion-Position-Sensors/_/N-6g7q6/
* https://www.mouser.es/Sensors/Liquid-Level-Sensors/_/N-zqig/
* https://www.mouser.es/Sensors/Pressure-Sensors/_/N-6g7qc/
* https://www.mouser.es/Sensors/Temperature-Sensors/_/N-7gz50/
* https://www.mouser.es/Sensors/Capacitive-Touch-Sensors/_/N-1b8oy/

* http://openbci.com/

# TFG en relación al mio

* http://bibing.us.es/proyectos/abreproy/91256/fichero/Estudio_y_Dise%C3%B1o_de_un_Sistema_Interfaz_Cerebro_Computador_de_Bajo_Coste_con_M%C3%B3dulo_Arduino.pdf

* http://oa.upm.es/37305/7/PFC_SANTIAGO_CANAS_CASCO_2015.pdf

## Componentes del sistema

* https://sites.google.com/site/chipstein/home-page/eeg-with-an-arduino

* https://sites.google.com/site/chipstein/home-page/eeg-with-an-arduino/recording-eeg-or-ekg-with-an-arduino

* https://sites.google.com/site/chipstein/home-page/eeg-with-an-arduino/recording-eeg-or-ekg-with-an-arduino



### Tiendas de circuitos electronicos ###

* https://es.rs-online.com/web/?gclid=Cj0KCQjwvezZBRDkARIsADKQyPl0p66WaRncGCN5Tg1mxGhQHLR989a7hcApLoaNA_wTNj19xS6b3XQaAo71EALw_wcB&cm_mmc=ES-PPC-DS3A-_-google-_-1_ES_ES_G_Generics_Exact-_-Electronics%7CVariations-_-componentes+electronicos&matchtype=e&gclsrc=aw.ds

* http://www.cetronic.es/sqlcommerce/disenos/plantilla1/seccion/Catalogo.jsp?idIdioma=&idTienda=93&cPath=&gclid=Cj0KCQjwvezZBRDkARIsADKQyPlITsjYraM0INAih3Bc5NmBOgP1ZHPwLtaJxBPe1nXpre0VopGlmcMaApsvEALw_wcB

* http://odielelectronica.com/

* https://www.mouser.es/

* https://diotronic.com/

# Información sobre la migraña #

* https://www.sanitas.es/sanitas/seguros/es/particulares/biblioteca-de-salud/prevencion-salud/migrana-jaqueca.html
* http://www.neurofeedback.cat/migranas-y-dolores-de-cabeza-mnu-25
* http://www.sonidosbinaurales.com/lista-completa-de-ondas-cerebrales-y-frecuencias-de-onda-en-el-ser-humano/
<<<<<<< Updated upstream
* https://omicrono.elespanol.com/2016/05/descubren-el-origen-de-la-migrana-en-el-cerebro/

=======
* https://sites.google.com/site/cerebrohumanoycalculoracional/neuroimagen/hemoencefalografia
>>>>>>> Stashed changes

# Información sobre Boostrap #

* https://librosweb.es/libro/bootstrap_3/capitulo_1.html
* http://getbootstrap.com/
* http://getbootstrap.com/docs/4.1/getting-started/introduction/