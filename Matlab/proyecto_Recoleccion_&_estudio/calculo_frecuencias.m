%% Modulo de la programación del gráfico de barras de las diferentes 
% frecuencias de estudio en el TFG

%% Declaración de las diferentes frecuencias de estudio
alpha=0;beta=0;delta=0;gamma=0;theta=0;
%% Declaración de la frecuencia total, sumatoria de todas
total=0;

% Sumatorio del potencial de todas las frecuencias.
cont=1;
cont25=1;
for i=1:length(f)
    if f(i) >= 0.5
        %total=total+(2*abs(Y(cont)));
        total = total + P1(cont);
    end
end

%% Sumatorio de las frecuencaias Delta
cont=1;
while f(cont)<4
    if(f(cont)<0.6)
        cont=cont+1;
    else
%         delta = delta+(2*abs(Y(cont)));
        delta = delta + P1(cont);
        cont=cont+1;
    end
end


%% Sumatorio de las frecuencaias Theta
while f(cont)<=8
    %theta = theta+(2*abs(Y(cont)));
    theta = theta + P1(cont);
    cont=cont+1;
end

%% Sumatorio de las frecuencaias Alpha
while f(cont)<=13
    %alpha = alpha+(2*abs(Y(cont)));
    alpha = alpha + P1(cont);
    cont=cont+1;
end

llega_cont = 1;
%% Sumatorio de las frecuencaias Beta
while f(cont)<=30
%     if(f(cont)>=25 && f(cont)<26 && llega_cont==1)
%         cont25=cont;
%         llega_cont=0;
%     end
    %beta = beta+(2*abs(Y(cont)));
    beta = beta + P1(cont);
    cont = cont+1;
end

% cont=cont25;
%% Sumatorio de las frecuencaias Gamma
while f(cont)<=60.5
    %gamma = gamma+(2*abs(Y(cont)));
    gamma = gamma + P1(cont);
    cont = cont+1;
end

alpha = alpha/total;
beta = beta/total;
gamma = gamma/total;
theta = theta/total;
delta = delta/total;

figure(3);
frecuencies = [delta theta alpha beta gamma];
c = {'delta','theta','alpha','beta','gamma'};
h=bar(frecuencies);
set(gca,'XTickLabel',c)