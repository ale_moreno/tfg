%% %% Window x and overlap y
%% Mirar primero la cantidad de datos tomada 
% y apartir de ah� elegir la ventana de estudio y el solapamiento
%% Frecuencia m�xima a estudiar = 100 Hz (Fs/2)

Fs = 200;


%% Estudio con ventana decimaparte de la muestra
w = i/10;
Window = round(w);

% Solapamiento es la ventana
overlap = Window;
figure
spectrogram(data,i,Window,overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);

% Solapamiento es la ventana menos 5
overlap = Window-5;
figure
spectrogram(data,i,Window,overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);

% Solapamiento es la mitad de la ventana
figure,
Overlap = round(Window/2)
spectrogram(data,i,Window,Overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(Overlap)]);

% Solapamiento es la quinta parte de la ventana
figure,
Overlap = round(Window/5)
spectrogram(data,i,Window,Overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(Overlap)]);

%% Estudio con ventana quintaparte

Window=round(i/5)

% Solapamiento es la ventana
overlap = Window;
figure
spectrogram(data,i,Window,overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);

% Solapamiento es la ventana menos 5
overlap = Window-5;
figure
spectrogram(data,i,Window,overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);

% Solapamiento es la mitad de la ventana
figure,
Overlap = round(Window/2)
spectrogram(data,i,Window,Overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(Overlap)]);

% Solapamiento es la quinta parte de la ventana
figure,
Overlap = round(Window/5)
spectrogram(data,i,Window,Overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(Overlap)]);


%% Estudio con ventana mitad
Window = round(i/2)
% Solapamiento es la ventana
overlap = Window;
figure
spectrogram(data,i,Window,overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);

% Solapamiento es la ventana menos 5
overlap = Window-5;
figure
spectrogram(data,i,Window,overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);

% Solapamiento es la mitad de la ventana
figure,
Overlap = round(Window/2)
spectrogram(data,i,Window,Overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(Overlap)]);

% Solapamiento es la quinta parte de la ventana
figure,
Overlap = round(Window/5)
spectrogram(data,i,Window,Overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(Overlap)]);

%% Estudio con ventana muestral menos 1
Window = i-1
% Solapamiento es la ventana
overlap = Window;
figure
spectrogram(data,i,Window,overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);

% Solapamiento es la ventana menos 5
overlap = Window-5;
figure
spectrogram(data,i,Window,overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(overlap)]);

% Solapamiento es la mitad de la ventana
figure,
Overlap = round(Window/2)
spectrogram(data,i,Window,Overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(Overlap)]);

% Solapamiento es la quinta parte de la ventana
figure,
Overlap = round(Window/5)
spectrogram(data,i,Window,Overlap,Fs)
title(['Windows = ' num2str(Window) ' & Overlap =' num2str(Overlap)]);


