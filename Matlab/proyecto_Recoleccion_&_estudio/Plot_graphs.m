figure(1);
plot(m);
title('EEG graph data');
drawnow;

figure(2);
pwelch(m,[],[],[],Fs);
title('Data Frecuency graph');
drawnow;

figure(3);
spectrogram(m,[],[],[],Fs,'yaxis');
title('Datas Spectrogram STFT');
drawnow;

figure(4);
frecuencies = [delta theta alpha beta gamma];
c = {'delta','theta','alpha','beta','gamma'};
h=bar(frecuencies);
set(gca,'XTickLabel',c)