% Filter m with 60 Hz notch
Wo = 60/(Fs/2);  
BW = Wo/35;
[b,a] = iirnotch(Wo,BW);
m_filtered = filter(b,a,m);


% Remove DC offset
mu = mean(m_filtered);
m_filtered_lessmu = m_filtered - mu + 1024/2;


%% DFT
% L=length(m_filtered_lessmu);
% NFFT = 2^nextpow2(L); % Next power of 2 from length of y
% Y = fft(m_filtered_lessmu,NFFT)L;
% %f = Fs/2*linspace(0,1,NFFT/2+1);
% f = Fs * (0:(NFFT/2))/NFFT;

% figure(4);
% plot(f,abs(Y(1:NFFT/2+1)))
% title('Single-Sided Amplitude Spectrum of y(t)')
% xlabel('Frequency (Hz)')
% ylabel('|Y(f)|') 


%% FFT
L=length(m_filtered_lessmu);
Y = fft(m_filtered_lessmu);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

figure(4);
plot(f(4:4:end),P1(4:4:end));
figure(5);
stem(f(4:4:end),P1(4:4:end));
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|P(f)| MAgnitud dB') 

calculo_frecuencias;