figure(1);
plot(m);
ylabel('Signal potency (13-bits)');
xlabel('N� Datas recolected');


L=length(m_filtered_lessmu);
Y = fft(m_filtered_lessmu);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

figure(4);
plot(f(4:4:end),P1(4:4:end));
figure(5);
stem(f(4:4:end),P1(4:4:end));
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|P(f)| MAgnitud dB') 

figure(3);
calculo_frecuencias;