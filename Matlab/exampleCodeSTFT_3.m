
%% Window 200 and overlap 128
figure,
spectrogram(data,i,200,128,200)
title('windows = 200 & Noverlap = 128');

figure,
spectrogram(data,i,10,5,200)
title('windows = 10 & Noverlap = 5');

figure,
spectrogram(data,i,400,200,200)
title('windows = 400 & Noverlap = 200');

figure,
spectrogram(data,i,250,100,200)
title('windows = 250 & Noverlap = 100');