fs = 1000;
t = 0:1/fs:2;
y = sin(128*pi*t); + sin(256*pi*t); % sine of periods 64 and 128.
level = 6;
plot (t,y);
title('Simple fuction');

figure;
windowsize = 128;
window = hanning(windowsize);
nfft = windowsize;
noverlap = windowsize-1;
spectrogram(y,window,noverlap,nfft,fs);
title('Short-time Fourier Transform spectrum')