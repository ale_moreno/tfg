clear all
clc

t = 0:0.001:2;
x = chirp(t,100,1,200,'quadratic');
spectrogram(x,blackman(128),60,128,1e3)
ax = gca;
ax.YDir = 'reverse';

[s,f,t]=spectrogram(x,blackman(128),60,128,1e3)